// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyCzTjjQPwyc34Px76KeLLjXEbKrxWxTsNY",
    authDomain: "covid-767f9.firebaseapp.com",
    databaseURL: "https://covid-767f9.firebaseio.com",
    projectId: "covid-767f9",
    storageBucket: "covid-767f9.appspot.com",
    messagingSenderId: "516332146497",
    appId: "1:516332146497:web:2fc30342a221ec46a41bc0",
    measurementId: "G-1LDXFNQW5C"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();
// Retrieve Firebase Messaging object.
const messaging = firebase.messaging();
// Add the public key generated from the console here.
messaging.requestPermission()
    .then(function () {
        console.log('Have Permission');
        return messaging.getToken();
    })
    .then(function (token) {
        console.log(token);
    })
    .catch(function (error) {
        console.log(error);
    });

messaging.onMessage(function (payload) {
console.log('on Message');
});
