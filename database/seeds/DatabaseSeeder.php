<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CategoryTableSeed::class);
        $this->call(CircleTableSeeder::class);
        $this->call(NewsTableSeeder::class);
        $this->call(PostTableSeeder::class);
        $this->call(QuestionTableSeeder::class);
        $this->call(HealthRecordTableSeeder::class);
        $this->call(GeoSeeder::class);
        $this->call(CountryTableSeeder::class);
    }
}
