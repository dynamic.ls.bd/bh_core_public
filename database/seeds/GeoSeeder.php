<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GeoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        $path = 'app/docs/db_geocode.sql';
        DB::unprepared(file_get_contents($path));
        //$this->command->info('geocode seeded!');
    }
}
