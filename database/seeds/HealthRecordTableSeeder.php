<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HealthRecordTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('health_records')->insert([
            'user_id' => 2,
            'date' => '2020-03-12',
            'temperature' => '98',
            'cough' => 'no',
            'sneezing' => 'no',
            'breathing' => 'no',
            'pain' => 'yes',
            'hand_wash' => 'yes',
            'notes' => 'All is well',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
