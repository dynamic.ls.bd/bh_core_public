<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QuestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questions')->insert([
            'to' => 'Mahedi Hasan',
            'subject' => 'This is first question',
            'message' => 'Corona beer bottles · What the Dubious Corona Poll Reveals ... We want to hear what you think about this article. Submit a letter to the edito',
            'created_by' => 1,
            'unread' => 0,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('questions')->insert([
            'to' => 'Jhon Deo',
            'subject' => 'This is second question?',
            'message' => 'Corona beer bottles · What the Dubious Corona Poll Reveals ... We want to hear what you think about this article. Submit a letter to the edito',
            'created_by' => 2,
            'unread' => 0,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        //feedback
        DB::table('feedback')->insert([
            'user_id' => 1,
            'feedbacktable_id' => 1,
            'feedbacktable_type' => 'App\Question',
            'feedback' => 'Fears grow over joke UK response as infections soar',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('feedback')->insert([
            'user_id' => 2,
            'feedbacktable_id' => 2,
            'feedbacktable_type' => 'App\Question',
            'feedback' => 'Fears grow over joke UK response as infections soar',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
