<?php

use Illuminate\Database\Seeder;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        $countries = 'app/docs/countries.sql';
        DB::unprepared(file_get_contents($countries));
    }
}
