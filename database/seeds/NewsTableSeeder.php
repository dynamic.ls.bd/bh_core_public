<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('news')->insert([
            'title' => 'First news of corona',
            'slug' => 'first-news',
            'user_id' => 1,
            'content' => 'Some of the important features about Allow CORS: Access-Control-Allow-origin addon are listed below. This is followed by an overview about this addon. This summary contains few words about the addon, its function and features.',
            'image' => 'images/default.png',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('news')->insert([
            'title' => 'Second news of corona',
            'slug' => 'second-news',
            'user_id' => 1,
            'content' => 'Some of the important features about Allow CORS: Access-Control-Allow-origin addon are listed below. This is followed by an overview about this addon. This summary contains few words about the addon, its function and features.',
            'image' => 'images/default.png',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('news')->insert([
            'title' => 'Coronavirus confirmed as pandemic by World Health',
            'slug' => 'coronavirus-confirmed-as-pandemic',
            'user_id' => 1,
            'content' => 'The coronavirus outbreak has been labelled a pandemic by the World Health Organization (WHO). WHO chief Dr Tedros Adhanom. Some of the important features about Allow CORS: Access-Control-Allow-origin addon are listed below. This is followed by an overview about this addon. This summary contains few words about the addon, its function and features.',
            'image' => 'images/default.png',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('news')->insert([
            'title' => 'Current information about the novel coronavirus',
            'slug' => 'current-information-about-the-novel-coronavirus',
            'user_id' => 1,
            'content' => 'Current information about the novel coronavirus. The coronavirus outbreak has been labelled a pandemic by the World Health Organization (WHO). WHO chief Dr Tedros Adhanom. Some of the important features about Allow CORS: Access-Control-Allow-origin addon are listed below. This is followed by an overview about this addon. This summary contains few words about the addon, its function and features.',
            'image' => 'images/default.png',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
