<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            'title' => 'The Corona Nature',
            'category_id' => 1,
            'slug' => 'the-corona-nature',
            'content' => 'Different from both MERS-CoV and SARS-CoV, 2019-nCoV is the seventh member of the family of coronaviruses that infect humans. Enhanced',
            'image' => 'images/default.png',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('posts')->insert([
            'title' => 'Coronavirus (Covid-19) NEJM',
            'category_id' => 2,
            'slug' => 'corona-covid-19',
            'content' => 'Different from both MERS-CoV and SARS-CoV, 2019-nCoV is the seventh member of the family of coronaviruses that infect humans. Enhanced',
            'image' => 'images/default.png',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('posts')->insert([
            'title' => 'Coronavirus: The Case for Canceling Everything',
            'category_id' => 3,
            'slug' => 'corona-he-case-for-canceling',
            'content' => 'Different from both MERS-CoV and SARS-CoV, 2019-nCoV is the seventh member of the family of coronaviruses that infect humans. Enhanced',
            'image' => 'images/default.png',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
