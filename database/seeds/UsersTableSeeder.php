<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name'              => 'Admin User',
            'email'             => 'admin@admin.com',
            'phone'             => '01720123456',
            'password'          => Hash::make('123456'),
        ]);

        $profile = new \App\Profile();
        $profile->user_id = $user->id;
        $profile->save();

    }
}
