<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoryTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'title' => 'About Covid 19',
            'description' => 'This is covid 19 description',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('categories')->insert([
            'title' => 'How I can be safe',
            'description' => 'Description How I can be safe',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('categories')->insert([
            'title' => 'Best Practices',
            'description' => 'Best Practices description',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('categories')->insert([
            'title' => 'Hand Wash',
            'description' => 'Description of Hand Wash',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
