<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CircleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('circles')->insert([
            'title' => 'Deo Homes',
            'description' => 'This is Deo Homes description',
            'category' => 'family',
            'created_by' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('circles')->insert([
            'title' => 'Friend Circle',
            'description' => 'This is Friend Circle description',
            'category' => 'friend',
            'created_by' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('circles')->insert([
            'title' => 'Related Circle',
            'description' => 'This is related description',
            'category' => 'related',
            'created_by' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('circles')->insert([
            'title' => 'Colleague Circle',
            'description' => 'This is related description',
            'category' => 'colleague',
            'created_by' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $user = User::create([
            'name'              => 'Deo User',
            'email'             => 'demo@demo.com',
            'phone'             => '01720123111',
            'password'          => Hash::make('123456'),
        ]);

        $profile = new \App\Profile();
        $profile->user_id = $user->id;
        $profile->save();

        //insert circle user
        DB::table('circle_users')->insert([
            'circle_id' => 1,
            'user_id' => 1,
            'isAdmin' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('circle_users')->insert([
            'circle_id' => 2,
            'user_id' => 2,
            'isAdmin' => 0,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
