<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCircleUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('circle_users', function (Blueprint $table) {
            $table->unsignedBigInteger('circle_id');
            $table->unsignedBigInteger('user_id');
            $table->string('relation')->nullable();
            $table->boolean('isAdmin')->default(false)->comment('false = not admin, true = admin');
            $table->timestamps();

            $table->unique(['circle_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('circle_users');
    }
}
