<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateNewFieldHealthRecord extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // for blood pressure records
        Schema::table('health_records', function (Blueprint $table) {
            $table->json('medication')->nullable()->after('hand_wash');
            $table->text('food_diets')->nullable()->after('medication');
            $table->integer('sleeping_hour')->nullable()->after('food_diets');
            $table->text('activities')->nullable()->after('sleeping_hour');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('health_records', function (Blueprint $table) {
            $table->dropColumn(['medication','food_diets','sleeping_hour', 'activities']);
        });
    }
}
