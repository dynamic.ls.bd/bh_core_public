<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUpperAndLowerPointToHealthRecord extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // for blood pressure records
        Schema::table('health_records', function (Blueprint $table) {
            $table->double('diastolic')->nullable()->after('id');
            $table->double('systolic')->nullable()->after('diastolic');
            $table->integer('pulse')->nullable()->after('systolic');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('health_records', function (Blueprint $table) {
            $table->dropColumn(['diastolic','systolic','pulse']);
        });
    }
}
