<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSomeFieldInDifferentMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->string('occupation')->after('user_id')->nullable();
            $table->string('job_address')->after('occupation')->nullable();
            $table->string('diabetes')->after('job_address')->nullable();
            $table->string('heart_disease')->after('diabetes')->nullable();
            $table->string('kidney_disease')->after('heart_disease')->nullable();
            $table->string('respiratory_disease')->after('kidney_disease')->nullable();
            $table->text('others')->after('respiratory_disease')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->dropColumn(['occupation','job_address','diabetes','heart_disease','kidney_disease','respiratory_disease','others']);
        });
    }
}
