<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSomeFieldToProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->unsignedBigInteger('upazila_id')->nullable()->after('id');
            $table->boolean('returned_from_abroad')->default(0)->comment('0 => no, 1 => yes')->after('upazila_id');
            $table->date('rba_date')->nullable()->after('returned_from_abroad');
            $table->unsignedBigInteger('rb_country_id')->nullable()->after('rba_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->dropColumn(['upazila_id','returned_from_abroad','rba_date','rb_country_id']);
        });
    }
}
