<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMeetingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meetings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('me')->nullable();
            $table->unsignedBigInteger('with_whom')->nullable();
            $table->tinyInteger('type')->comment('1 => individual, 2 => Meeting')->default(1);
            $table->integer('meeting_group')->nullable();
            $table->dateTime('date_time')->nullable();
            $table->double('lat',10,7)->nullable();
            $table->double('lng',10,7)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meetings');
    }
}
