<?php


return [
    "circle_category" => [
        "family" => "Family",
        "friend" => "Friend/Relative",
        "known" => "Known",
        "colleague" => "Colleague"
    ],
    "faq_categories" =>[
        'question' => 'Question',
        'comment' => 'Comment',
        'suggestion' => 'Suggestion',
        'contribution' => 'Contribution',
    ],
    "message_type" => [
        'voice' => 'Voice',
        'text' => 'Text'
    ],
    "affect_type" => [
        'infected' => 'Infected',
        'deceased' => 'Deceased',
        'sick' => 'Sick',
        'recovered' => 'Recovered',
    ],
    "pain_type" => [
        'headache' => 'Headache',
        'sore_throat' => 'Sore throat',
    ],
    "observation_type" => [
        'Observation',
        'Comments',
        'Learnings'
    ],
    "observation_to" => [
        'Civil Surgeon',
        'Superintendent',
        'Doctor',
        'Nurse',
        'Technician'
    ],
    "observation_tag" => [
        'Medicine',
        'Wash',
        'Breathing Management',
        'Ventilator',
        'Improvement'
    ],
    "default_notification" => [
        0 => [ 
                'id' => 1,
                'title' => 'আপনার মোবাইলে BH অ্যাপটি সফলভাবে ইন্সটল হয়েছে',
                'message' => '',
                'read_at' => '1'
            ],
        1 => [ 
                'id' => 2,
                'title' => 'আপনার পরিচিতজনদের সেলফ-সার্কেলে যুক্ত করুন , এছাড়াও আপনি প্রতিদিন যাদের সংস্পর্শে এসেছেন এবং যেই স্থানে ভ্রমন করেছেন তা  সেলফ-সার্কেলে যুক্ত করুন । ভবিষ্যতের সুরক্ষা নিশ্চিত করতে সঠিক তথ্য  প্রদান করুন । ',
                'message' => '',
                'read_at' => '1'
            ],
        2 => [ 
                'id' => 3,
                'title' => 'আপনি বা আপনার পরিচিত কোনও ব্যক্তি যদি সেলফ-কোয়ারান্টিনে প্রবেশ করেন তবে আপনার চেনাশোনা সদস্য সহ  আপনিও নোটিফিকেশন পাবেন',
                'message' => '',
                'read_at' => '1'
            ],
        3 => [ 
                'id' => 4,
                'title' => 'যে কোনও জরুরি অবস্থার জন্য এসএমএস এবং ভয়েস এসএমএস প্রেরণে আমাদের অ্যাপ্লিকেশনটি ব্যবহার করুন',
                'message' => '',
                'read_at' => '1'
            ],
        4 => [ 
                'id' => 5,
                'title' => 'আপনার প্রতিদিনের স্বাস্থ্য রেকর্ডের ভিত্তিতে আপনি একজন প্রত্যয়িত ডাক্তারের মন্তব্য পাবেন',
                'message' => '',
                'read_at' => '1'
            ],
    ],
    "trim_length" => 100
];
