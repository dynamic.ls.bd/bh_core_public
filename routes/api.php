<?php

use Illuminate\Http\Request;


Route::get('/', function () {
    return [
        'app' => 'Laravel 6 API Template',
        'version' => '1.0.1',
    ];
});


Route::group(['namespace' => 'Auth'], function () {

    Route::post('auth/login', ['as' => 'login', 'uses' => 'AuthController@login']);

    Route::post('auth/register', ['as' => 'register', 'uses' => 'RegisterController@register']);
    // Send reset password mail
    Route::post('auth/recovery', 'ForgotPasswordController@sendPasswordResetLink');
    // handle reset password form process
    Route::post('auth/reset', 'ResetPasswordController@callResetPassword');

});

Route::group(['middleware' => ['jwt', 'jwt.auth']], function () {

    Route::group(['namespace' => 'Profile'], function () {

        Route::get('profile', ['as' => 'profile', 'uses' => 'ProfileController@me']);

        Route::post('profile', ['as' => 'profile', 'uses' => 'ProfileController@update']);

        Route::post('profile/password', ['as' => 'profile', 'uses' => 'ProfileController@updatePassword']);

    });

    Route::group(['namespace' => 'Auth'], function () {

        Route::post('auth/logout', ['as' => 'logout', 'uses' => 'LogoutController@logout']);

    });
    // Route for Quarentation
    Route::get('/quarentations','QuarentetionController@index');
    Route::post('/quarentations','QuarentetionController@store');
    Route::get('/quarentation/{id}','QuarentetionController@edit');
    Route::post('/quarentation-update','QuarentetionController@update');
    Route::post('/quarentation-delete','QuarentetionController@destroy');

    // Route for my circle
    Route::get('my-circle','CircleController@myCircles');
    Route::get('my-circle-wise-user','CircleController@myCircle');

    // Route for Visit
    Route::get('/visits','VisitController@index');
    Route::get('/my-visits','VisitController@myVisits');
    Route::get('/my-todays-visit','VisitController@myTodaysVisit');
    Route::post('/visits','VisitController@store');
    Route::get('/visit/{id}','VisitController@edit');
    Route::post('/visit/update','VisitController@update');
    Route::post('/visit/delete','VisitController@destroy');

    // Route for user search
    Route::post('/search-user','MeetingController@searchUser');

    // Route for user Notification
    Route::get('/notifications','NotificationController@getAllNotification');
    Route::get('/mark-as-read','NotificationController@markAllNotificationsAsRead');
    Route::post('/mark-as-read','NotificationController@markNotificationAsRead');

    // Route for Meeting
    Route::get('/meetings','MeetingController@index');
    Route::get('/my-meetings','MeetingController@myMeetings');
    Route::get('/my-todays-meeting','MeetingController@myTodaysMeetings');
    Route::post('/meetings','MeetingController@store');
    Route::get('/meeting/{id}','MeetingController@edit');
    Route::post('/meeting/user-delete','MeetingController@deleteMeetingUser');
    Route::post('/meeting/update','MeetingController@update');
    Route::post('/meeting/delete','MeetingController@destroy');

    // Route for Health Record
    Route::group(['prefix'=>'health_records'], function(){
        Route::get('index', 'HealthRecordController@index')->name('index.health_record');
        Route::post('own_records', 'HealthRecordController@ownRecords')->name('oun_records.health_record');
        Route::get('/{health_record}', 'HealthRecordController@show')->name('show.health_record');
        Route::post('store', 'HealthRecordController@store')->name('store.health_record');
        Route::post('update/{health_record}', 'HealthRecordController@update')->name('update.health_record');
        Route::post('destroy/{health_record}', 'HealthRecordController@destroy')->name('destroy.health_record');
    });

    // Route for protocols
    Route::group(['prefix'=>'protocols'], function(){
        Route::get('index', 'ProtocolController@index')->name('index.protocol');
        Route::get('/{protocol}', 'ProtocolController@show')->name('show.protocol');
    });

    // Route for Observation
    Route::group(['prefix'=>'observations'], function(){
        Route::get('index', 'ObservationController@index')->name('index.observation');
        Route::get('own', 'ObservationController@own')->name('own.observation');
        Route::post('store', 'ObservationController@store')->name('store.observation');
        Route::get('/{observation}', 'ObservationController@show')->name('show.observation');
    });

    // Route for announcements
    Route::group(['prefix'=>'announcements'], function(){
        Route::get('index', 'AnnouncementController@index')->name('index.announcement');
        Route::get('/{announcement}', 'AnnouncementController@show')->name('show.announcement');
    });

    // emergency route
    Route::get('/message-type','EmergencyMessageController@getMessageType');
    Route::get('/my-message','EmergencyMessageController@getMyMessage');
    Route::post('/message-store','EmergencyMessageController@store');

    //circle route
    Route::post('circle-store', ['as' => 'store', 'uses' => 'CircleController@store']);
    Route::get('circle/{id}', ['as' => 'show', 'uses' => 'CircleController@show']);
    Route::get('/circle-category', 'CircleController@circleCategory');
    Route::post('circle-update/{circle}', ['as' => 'update', 'uses' => 'CircleController@update']);
    //circle user route
    Route::post('circle-user-store', ['as' => 'store', 'uses' => 'CircleUserController@store']);
    Route::post('circle-user-edit', ['as' => 'edit', 'uses' => 'CircleUserController@edit']);
    Route::post('circle-user-update', ['as' => 'update', 'uses' => 'CircleUserController@update']);
    Route::post('circle-user-delete', ['as' => 'destroy', 'uses' => 'CircleUserController@destroy']);
    Route::post('circle-user-swap', 'CircleUserController@swapUser');

    //question route
    Route::get('my-questions', ['as' => 'myQuestion', 'uses' => 'QuestionController@myQuestion']);
    Route::get('question/{id}', ['as' => 'show', 'uses' => 'QuestionController@show']);
    Route::post('question-comment/{id}', ['as' => 'commentQuestion', 'uses' => 'QuestionController@commentQuestion']);
    //Suggestion
    Route::get('/suggestions', ['as' => 'index', 'uses' => 'SuggestionController@index']);
    Route::get('/my-suggestion', ['as' => 'mySuggestion', 'uses' => 'SuggestionController@mySuggestion']);
    Route::get('/suggestion/{id}', ['as' => 'show', 'uses' => 'SuggestionController@show']);
    Route::post('/suggestion-comment/{id}', ['as' => 'commentSuggestion', 'uses' => 'SuggestionController@commentSuggestion']);

    //contribution
    Route::get('contributions/', ['as' => 'index', 'uses' => 'ContributionController@index']);
    Route::get('my-contribution/', ['as' => 'myContribution', 'uses' => 'ContributionController@myContribution']);
    Route::get('contribution/{id}', ['as' => 'show', 'uses' => 'ContributionController@show']);
    Route::post('contribution-comment/{id}', ['as' => 'comment', 'uses' => 'ContributionController@comment']);
    //contribution
    Route::get('comments/', ['as' => 'index', 'uses' => 'CommentController@index']);
    Route::get('my-comment/', ['as' => 'myContribution', 'uses' => 'CommentController@myContribution']);
    Route::get('comment/{id}', ['as' => 'show', 'uses' => 'CommentController@show']);
    Route::post('comment-save/{id}', ['as' => 'comment', 'uses' => 'CommentController@comment']);

    // user feedback common routes
    Route::post('/list','FeedBackController@getAllData');
    Route::post('/all-user-feedback','FeedBackController@index');
    Route::post('/my-feedback','FeedBackController@getMyFeedBacks');
    Route::post('/store-user-feedback','FeedBackController@store');
    Route::post('/my-feedback-edit','FeedBackController@editMyFeedBack');
    Route::post('/my-feedback-update','FeedBackController@update');

    // news auth routes
    Route::post('/news','NewsController@store');
    Route::post('/category-wise-news','NewsController@getCategoryWiseNews');

    //Statistic
    Route::get('statistics','StatisticController@index');
    Route::post('statistic-store','StatisticController@store');
    Route::get('daily-confirmed-statistics','StatisticController@dailyConfirmed');
    Route::get('daily-cumulative-statistics','StatisticController@dailyCumulative');

    //Resource
    Route::get('resources','ResourcesController@index');

});

//public route
Route::get('category', ['as' => 'category', 'uses' => 'CategoryController@index']);
Route::get('category/{category}', ['as' => 'show', 'uses' => 'CategoryController@show']);
Route::get('news', ['as' => 'news', 'uses' => 'NewsController@index']);
Route::get('news/{news}', 'NewsController@show');
Route::get('questions', ['as' => 'questions', 'uses' => 'QuestionController@index']);
Route::get('circles', ['as' => 'index', 'uses' => 'CircleController@index']);

//config route
Route::get('/get-config/{value?}',['as' => 'configList', 'uses' => 'ConfigController@configList']);// optional parameters
Route::get('/get-associative-config/{value?}','ConfigController@index');// optional parameters
Route::get('/faq-categories','ConfigController@faqCategories');
Route::get('/affect-type','ConfigController@affectType');
// search country
Route::post('search-country', 'GeoController@searchCountry');
//search upazila
Route::post('search-upazila', 'GeoController@searchUpazila');

