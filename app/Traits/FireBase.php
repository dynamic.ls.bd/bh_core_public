<?php
namespace App\Traits;

trait FireBase
{
	protected function sendFireBaseNotification($token, $title, $message)
	{
	    $url = "https://fcm.googleapis.com/fcm/send";
	    $ligecyServerKey = "AIzaSyCzTjjQPwyc34Px76KeLLjXEbKrxWxTsNY"; // tanjil's ligecy server key
	//    $serverKey = 'AAAAeDfLT0E:APA91bHfGEewqtZAb6roWKecL6cuGDc4XjdAOdUuJ7OfjWRc7NzE4wlrgQtQ6PRX0bxdKmnGEPu6O0CTN51nh9RQXab115P5U6DlhR_qWOrFKxzj5zC9-_lijXNgAIUFlz-uTmovh1p1';
	    $body = $message;

	    $notification = array('title' => $title, 'text' => $body, 'sound' => 'default', 'badge' => '1');

	    $arrayToSend = array('to' => $token, 'notification' => $notification, 'priority' => 'high');
	    $json = json_encode($arrayToSend);

	    $headers = array();

	    $headers[] = 'Content-Type: application/json';

	    $headers[] = 'Authorization: key=' . $ligecyServerKey;

	    $ch = curl_init();

	    curl_setopt($ch, CURLOPT_URL, $url);

	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    //Send the request
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // for hiding unneccessary response outputing

	    $response = curl_exec($ch);
	    //Close request

	}
}