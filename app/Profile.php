<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $dates = ['dob'];
    protected $casts = ['gender' => 'boolean'];

    protected $guarded = [];

    public function country()
    {
    	return $this->belongsTo('App\Country','rb_country_id');
    }

    public function upazila()
    {
    	return $this->belongsTo('App\Upazila','upazila_id');
    }
}
