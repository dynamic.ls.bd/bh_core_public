<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
    public function user(){
        return $this->belongsTo(User::class,'me');
    }

    public function whom(){
        return $this->belongsTo(User::class,'with_whom');
    }

    public function quarentation()
    {
        return $this->belongsToMany(Quarentetion::class,'meeting_quarentation','meeting_id','quarentation_id');
    }
}
