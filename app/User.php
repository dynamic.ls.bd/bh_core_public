<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    const NONREGISTERED = 0;
    const REGISTERED = 1;

    const BANNED = 0;
    const ACTIVE = 1;
    const INACTIVE = 2;

    public static function statusLabel(){
        return [
            self::BANNED => 'Banned',
            self::ACTIVE => 'Active',
            self::INACTIVE => 'Inactive'
        ];
    }

    public function getStatusLabelAttribute(){
        return self::statusLabel()[$this->status];
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone', 'static', 'status'
    ];

    protected $appends = [
        'status_label', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','firebase_token',
    ];


    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Override the mail body for reset password notification mail.
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new \App\Notifications\MailResetPasswordNotification($token));
    }

    public function profile()
    {
        return $this->hasOne(Profile::class,'user_id');
    }

    public function healthRecords()
    {
        return $this->hasMany(HealthRecord::class, 'user_id', 'id');
    }

    public function circles(){ // should be circle user
        return $this->belongsToMany(Circle::class,'circle_users','user_id','circle_id')->withPivot('relation','isAdmin');
    }

    public function userCircles(){
        return $this->hasMany(Circle::class,'created_by');
    }

    public function getRoleAttribute(){
        return $this->profile()->exists() ? $this->profile->role : 'user';
    }

}
