<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HealthRecord extends Model
{
    protected $fillable = [
        "user_id",
        "date",
	    "temperature",
	    "cough",
	    "sneezing",
	    "breathing",
        "pain",
        "hand_wash",
        "pulse",
        "diastolic", // for lower point of blood pressure
        "systolic", // for upper point of blood pressure
        "medication",
        "food_diets",
        "sleeping_hour",
        "activities",
        "notes"
    ];

    public function getPainAttribute($value) {
        return array_map('trim', explode(',', $value));
    }

    public function setPainAttribute($value) {
        $this->attributes['pain'] = implode(", ", $value);
    }

    public function getMedicationAttribute($value) {
        return json_decode($value);
    }

    public function setMedicationAttribute($value) {
        $this->attributes['medication'] = json_encode($value);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
