<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\User;
use App\Traits\FireBase;

class QuarentineNotification extends Notification implements ShouldQueue
{
    use Queueable,FireBase;
    private $user,$quarentineUser;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user,User $quarentineUser)
    {
        $this->user = $user;
        $this->quarentineUser = $quarentineUser;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $title = $this->quarentineUser->name.' is In Self Quarentine';
        $this->sendFireBaseNotification($this->user->firebase_token,$title,'Be aware of Covid-19, Stay safe');
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title' => $this->quarentineUser->name.' is In Self Quarentine',
            'message' => 'To Prevent more spreading don\'t visit him/her without any kind of protection. You can call him/her to inspire'
        ];
    }
}
