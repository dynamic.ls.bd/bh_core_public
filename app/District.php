<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    public function upazilas(){
        return $this->hasMany('App\Upazila');
    }

    public function divisions(){
        return $this->belongsTo('App\Division','division_id');
    }
}
