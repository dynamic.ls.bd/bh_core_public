<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quarentetion extends Model
{

    public function setActivityAttribute($value)
    {
        return $this->attributes['activity'] = implode('|',$value);
    }
    public function setSufferingsAttribute($value)
    {
        return $this->attributes['sufferings'] = implode('|',$value);
    }

    public function getActivityAttribute($value)
    {
        return explode('|',$value);
    }

    public function getSufferingsAttribute($value)
    {
        return explode('|',$value);
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function meetings(){
        return $this->belongsToMany(Meeting::class,'meeting_quarentation','quarentation_id','meeting_id');
    }
}
