<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Protocol extends Model
{
    protected $guarded = [];

    public function author(){
    	return $this->belongsTo('App\User','user_id');
    }
}
