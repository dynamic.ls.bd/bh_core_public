<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statistic extends Model
{
    protected $guarded = [];

    public function createdBy()
    {
        return $this->belongsTo(User::class,'created_by');
    }

    public function upazila()
    {
        return $this->belongsTo('App\Upazila','upazila_id');
    }

    public function division()
    {
        return $this->hasManyThrough(Division::class,Upazila::class);
    }
}
