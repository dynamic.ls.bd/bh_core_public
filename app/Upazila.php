<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upazila extends Model
{
    public function district(){
        return $this->belongsTo('App\District','district_id');
    }

    public function statistics()
    {
        return $this->hasMany(Statistic::class,'upazila_id');
    }

    public function division()
    {
        return $this->hasOneThrough(
        	'App\Division',
        	'App\District',
        	'id',// local key for current model
        	'id', // local key for district model
        	'district_id', // foreign key for current model
        	'division_id' // foreign key for district model
        );
    }
}
