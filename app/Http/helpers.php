<?php


use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

function storeImage($file, $folder = 'uploads', $width = 800, $height = 800)
{
    $path = $file->hashName('public/' . $folder);
    $serverPath = $file->hashName($folder);

    $image = Image::make($file)->resize($width, $height);

    Storage::put($path, (string)$image->encode());
    return $serverPath;
}

function storeOriginalImage($file, $folder = 'uploads')
{
    $path = $file->hashName('public/' . $folder);
    $serverPath = $file->hashName($folder);

    $image = Image::make($file);

    Storage::put($path, (string)$image->encode());

    return $serverPath;
}

function storeThumb($file,$width = 250,$height = 250,$imagePath = 'profile')
{
    $server = $imagePath."/" . md5(Str::random(40).time()) .".".$file->getClientOriginalExtension();
    $path = "public/" . $server;

    $image = Image::make($file)->resize($width,$height);

    Storage::put($path, (string)$image->encode());
    return $server;
}

function imagePath(){
    return asset('storage').'/';
}

function apiResponse($success = true,$message = null, $data = [],$status = null)
{
    $responseMessage = $message ? $message : 'failed!';
    $responseStatus = $status ? $status : 400;
    if($success){
        $responseMessage = $message ? $message : 'success!';
        $responseStatus = $status ? $status : 200;
    }
    $responseData = ['Success' => $success, 'message' => $responseMessage];
    if(!empty($data))
        $responseData['data'] = $data;
    return response()->json($responseData)->setStatusCode($responseStatus);
}
function apiResponseWithPagination($success = true,$message = null, $data = [], $paginator = null ,$status = null)
{
    $responseMessage = $message ? $message : 'failed!';
    $responseStatus = $status ? $status : 400;
    if($success){
        $responseMessage = $message ? $message : 'success!';
        $responseStatus = $status ? $status : 200;
    }
    $responseData = ['Success' => $success, 'message' => $responseMessage];
    if(!empty($paginator)){
        $paginate = array();
        $paginate['total'] = $paginator->total();
        $paginate['per_page'] = $paginator->perPage();
        $paginate['current_page'] = $paginator->currentPage();
        $paginate['last_page'] = $paginator->lastPage();
        $paginate['first_page_url'] = ($paginator->hasMorePages()) ? $paginator->url(1) : null;
        $paginate['last_page_url'] = ($paginator->hasMorePages()) ? $paginator->url($paginator->lastPage()) : null;
        $paginate['next_page_url'] = $paginator->nextPageUrl();
        $paginate['prev_page_url'] = $paginator->previousPageUrl();
        $responseData['pagination'] = $paginate;
    }
    if(!empty($data))
        $responseData['data'] = $data;
    return response()->json($responseData)->setStatusCode($responseStatus);
}

function storeAudioMessage($file, $folder = 'emergency_message')
{
    $path = $file->hashName('public/' . $folder);
    $serverPath = $file->hashName($folder);

    Storage::put($path, file_get_contents($file));

    return $serverPath;
}

function trimText($input, $length, $ellipses = true, $strip_html = true)
{
    //strip tags, if desired
    if ($strip_html) {
        $input = strip_tags($input);
    }

    //no need to trim, already shorter than trim length
    if (strlen($input) <= $length) {
        return $input;
    }

    //find last space within length
    $last_space = strrpos(substr($input, 0, $length), ' ');
    if ($last_space !== false) {
        $trimmed_text = substr($input, 0, $last_space);
    } else {
        $trimmed_text = substr($input, 0, $length);
    }
    //add ellipses (...)
    if ($ellipses) {
        $trimmed_text .= '...';
    }

    return $trimmed_text;
}