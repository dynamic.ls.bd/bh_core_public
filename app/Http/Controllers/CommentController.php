<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Feedback;
use App\Http\Resources\CommentResource;
use App\Http\Resources\CommentWithFeedback;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class CommentController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index()
    {
        $comments = Comment::paginate();
        if($comments->isNotEmpty())
            return apiResponseWithPagination(true,'Comments List',CommentResource::collection($comments),$comments);
        return apiResponse(true,'No Data Found');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'message' => 'required',
        ]);

        $created = Comment::create([
            'to' => $request->to,
            'subject' => $request->subject,
            'message' => $request->message,
            'created_by' => request()->user()->id
        ]);

        if ($created){
            return apiResponse(true,'Comment Successfully Created');
        }
        return apiResponse(false,'Failed to Create');
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $hasComment = Comment::with('feedbacks')->find($id);
        if (!empty($hasComment))
            return apiResponse(true,'Comment',new CommentWithFeedback($hasComment));
        return apiResponse(true,'No Data Found');
    }

    public function comment(Request $request, $id)
    {
        $this->validate($request,[
            'feedback' => 'required'
        ]);

        $suggestion = Comment::find($id);
        if(!empty($suggestion)){
            $save = new Feedback();
            $save->user_id = request()->user()->id;
            $save->feedback = $request->feedback;

            if ($suggestion->feedbacks()->save($save))
                return apiResponse(true,'Comment Successfully Created');
        }
        return apiResponse(false,'Failed to Create or No Data Found');
    }

    public function myContribution()
    {
        $comments = Comment::where('created_by', request()->user()->id)->paginate();
        if($comments->isNotEmpty())
            return apiResponseWithPagination(true,'My Comment List',CommentResource::collection($comments),$comments);
        return apiResponse(true,'No Data Found');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        //
    }
}
