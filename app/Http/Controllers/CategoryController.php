<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Resources\Category as CategoryResource;
use App\Http\Resources\Post as PostResource;

class CategoryController extends Controller
{
    public function index()
    {
        $data = Category::all();
        if($data->isNotEmpty())
            return apiResponse(true,'Category List',CategoryResource::collection($data));
        return apiResponse(true,'No Data Found');
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Category $category)
    {
        if($category){
            $data = Post::where('category_id','=',$category->id)->orderBy('id','desc')->paginate();
            if($data->isNotEmpty())
                return apiResponseWithPagination(true,'Category with Post',PostResource::collection($data),$data);
        }
        return apiResponse(true,'No Data Found');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
        public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
