<?php

namespace App\Http\Controllers;

use App\EmergencyMessage;
use Illuminate\Http\Request;

class EmergencyMessageController extends Controller
{
    public function getMyMessage()
    {
        $messages = EmergencyMessage::where('user_id','=',request()->user()->id)->get();
        if($messages->isNotEmpty())
            return apiResponse(true, 'Your Messaging list', $messages);
        return apiResponse(true, 'No Data Found');
    }

    public function getMessageType()
    {
        return apiResponse(true,'Message Type',config('arrays.message_type'));
    }

    public function store(Request $request)
    {
        $this->validator($request);
        $emergency = new EmergencyMessage();
        $emergency->user_id = request()->user()->id;
        $emergency->type = $request->type;
        if($request->type === 'text')
            $emergency->message = $request->message;
        if($request->type === 'voice'){
            if($request->hasFile('message')){
                $emergency->message = storeAudioMessage($request->file('message'));
            }
        }
        if($emergency->save())
            return apiResponse(true,'Successfully Created');
        return apiResponse(true,'Failed to Create');
    }

    private function validator($request)
    {
        $this->validate($request, [
            'message' => 'required|'.$this->getRule($request),
            'type' => 'required|string',
        ],[
            'message.required' => 'The message is required.',
            'message.string' => 'The message must be a string.',
            'message.file' => 'The message must be File.',
            'message.mimes' => 'The message must be amr or mp3 or wav File.',
        ]);
    }

    private function getRule($value){
        if($value->hasFile('message')){
            return 'file'; // |mimes:audio/mpeg,mpga,mp3,wav,aac,amr'
        }
        return 'string';
    }

    public function show(EmergencyMessage $emergencyMessage)
    {
        //
    }

    public function edit(EmergencyMessage $emergencyMessage)
    {
        //
    }

    public function update(Request $request, EmergencyMessage $emergencyMessage)
    {
        //
    }

    public function destroy(EmergencyMessage $emergencyMessage)
    {
        //
    }
}
