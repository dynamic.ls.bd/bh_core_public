<?php

namespace App\Http\Controllers;

use App\Feedback;
use App\Observation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\ObservationResource;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Resources\ObservationWithFeedback;

class ObservationController extends Controller
{
    /**
     * @return mixed
     */
    public function index()
    {
        $observations = Observation::paginate();

        if($observations->isNotEmpty())
            return apiResponseWithPagination(true,'My Observation List', ObservationWithFeedback::collection($observations), $observations);

        return apiResponse(true,'No Data Found');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'type' => 'required',
            'tags' => 'nullable|array',
            'message' => 'required'
        ]);

        $created = Observation::create([
           'type' => $request->type,
           'to' => $request->to,
           'tags' => $request->tags,
           'message' => $request->message,
           'user_id' => request()->user()->id
        ]);

        if ($created){
            return apiResponse(true,'Observation Successfully Stored');
        }
        return apiResponse(false,'Observation Failed to Store');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $observation = Observation::with('feedbacks')->findOrFail($id);

        if(!empty($observation))
            return apiResponse(true,'Observation with Feedback',new ObservationWithFeedback($observation));

        return apiResponse(true,'Data Not Found');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function commentObservation(Request $request, $id)
    {
        $this->validate($request,[
           'feedback' => 'required'
        ]);

        $observation = Observation::find($id);

        $feedback = new Feedback();
        $feedback->user_id = request()->user()->id;
        $feedback->feedback = $request->feedback;

        if ( $observation->feedbacks()->save($feedback)){
            return response()->json(['data' => 'Success'], 201);
        }

        return response()->json(['data' => 'fail to created'], 404);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Observation  $observation
     * @return \Illuminate\Http\Response
     */
    public function edit(Observation $observation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Observation  $observation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Observation $observation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Observation  $observation
     * @return \Illuminate\Http\Response
     */

    public function destroy(Observation $observation)
    {
        //
    }

    /**
     * @return mixed
     */
    public function own()
    {
        $observations = Observation::where('user_id', request()->user()->id)->paginate();
        if($observations->isNotEmpty())
            return apiResponseWithPagination(true,'My Observation List',ObservationWithFeedback::collection($observations),$observations);
        return apiResponse(true,'No Data Found');
    }
}
