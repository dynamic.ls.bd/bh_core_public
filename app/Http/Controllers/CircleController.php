<?php

namespace App\Http\Controllers;

use App\Circle;
use App\CircleUser;
use App\Http\Resources\CircleResource;
use App\Http\Resources\CircleUserResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class CircleController extends Controller
{
    public function index()
    {
        $data = Circle::paginate();
        if($data->isNotEmpty())
            return apiResponseWithPagination(true,'All Circles',CircleResource::collection($data),$data);
        return apiResponse(true,'No Data Found');
    }


    public function circleCategory()
    {
        return apiResponse(true,'Circle Category List',config('arrays.category'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'category' => 'required'
        ]);
        $authId = request()->user()->id;

        $circleExit = Circle::where('category', $request->category)->where('created_by','=',$authId)->exists();

       if (!$circleExit){
           DB::beginTransaction();
           try {
               $circle = new Circle();
               $circle->title = $request->title;
               $circle->description = $request->description;
               $circle->category = $request->category;
               $circle->created_by = $authId;

               $circle->save();

               CircleUser::create([
                   'circle_id' => $circle->id,
                   'user_id' => $authId,
                   'isAdmin' => 1
               ]);

               DB::commit();
               return apiResponse(true,'Successfully Created');

           } catch (\Exception $e) {
               DB::rollBack();
               return apiResponse(false,'Failed to Create');
           }
       }

       return apiResponse(true,'Selected category circle already created');
    }

    public function show($id)
    {
        $data = Circle::with('createdBy','users')->find($id);
        if(!empty($data))
            return apiResponse(true,'Circle Details',new CircleUserResource($data));
        return apiResponse(true,'No Data Found');
    }

    public function update(Request $request, Circle $circle)
    {
        if(!empty($circle)){
            $updated = $circle->update([
                'title' => $request->title,
                'description' => $request->description
            ]);
            if ($updated)
                return apiResponse(true,'Successfully Updated');
        }
        return apiResponse(false,'Failed to Updated');
    }

    public function myCircles()
    {
        $myCircles = Circle::with('createdBy','users')->where('created_by','=',request()->user()->id)->paginate();
        if($myCircles->isNotEmpty())
            return apiResponseWithPagination(true,'My Circle List',CircleUserResource::collection($myCircles),$myCircles);
        return apiResponse(true,'No Data Found');
    }

    public function myCircle(Request $request)
    {
        $this->validate($request,[
            'category' => 'required|string'
        ]);
        $myCircles = Circle::with('createdBy','users')
            ->where('created_by','=',request()->user()->id)
            ->where('category','=',$request->category)
            ->first();
        if(!empty($myCircles))
            return apiResponse(true,'Circle Wise Member',new CircleUserResource($myCircles));
        return apiResponse(true,'No Data Found');
    }
}
