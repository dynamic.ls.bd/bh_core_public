<?php

namespace App\Http\Controllers;

use App\Http\Resources\QuarentationResource;
use App\Quarentetion;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Notifications\QuarentineNotification;

class QuarentetionController extends Controller
{

    public function index()
    {
        $quarentation = Quarentetion::with('meetings')->where('user_id','=',request()->user()->id)->paginate();
        if($quarentation->isNotEmpty())
            return apiResponseWithPagination(true,'My quarentation list',QuarentationResource::collection($quarentation),$quarentation);
        return apiResponse(true,'No Data Found');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'day' => 'nullable|string',
            'date' => 'required|string',
            'activity' => 'nullable|array',
            'sufferings' => 'nullable|array',
        ]);
        $authId = request()->user()->id;
        $quarentation = new Quarentetion();
        $quarentation->day = $request->day;
        $quarentation->user_id = $authId;
        $quarentation->date = $request->date;
        $quarentation->activity = $request->activity;
        $quarentation->sufferings = $request->sufferings;
        if($quarentation->save()){
            $this->sendNotification($authId);
            return apiResponse(true,'Data Successfully Saved');
        }
        return apiResponse(false,'Failed to Create');
    }

    private function sendNotification($id){
        $authUser = request()->user();
        $user = User::with('circles.users')->find($id);
        if($user->circles()->exists()){
            foreach ($user->circles as $circle) {
                foreach ($circle->users as $user) {
                    if($user->firebase_token)
                        $authUser->notify((new QuarentineNotification($user,$authUser))->delay(now()->addSecond(5)));
                }
            }
        }
        return true;
    }


    public function edit($id)
    {
        $quarentation = Quarentetion::find($id);
        if(!empty($quarentation))
            return apiResponse(true,'Edit Quarentation',new QuarentationResource($quarentation));
        return apiResponse(true,'No Data Found');
    }

    public function update(Request $request)
    {
        $this->validate($request,[
            'day' => 'nullable|string',
            'date' => 'required|string',
            'quarentation_id' => 'required|numeric',
            'activity' => 'nullable|array',
            'sufferings' => 'nullable|array',
        ]);
        $quarentation = Quarentetion::find($request->quarentation_id);
        if(!empty($quarentation)){
            $quarentation->day = $request->day;
            $quarentation->date = $request->date;
            $quarentation->activity = $request->activity;
            $quarentation->sufferings = $request->sufferings;
            if($quarentation->save())
                return apiResponse(true,'Data Successfully Updated');
        }
        return apiResponse(false,'Failed to Update');
    }

    public function destroy(Request $request)
    {
        $this->validate($request,[
            'quarentation_id' => 'required|numeric',
        ]);
        $quarentation = Quarentetion::find($request->quarentation_id);
        if(!empty($quarentation)){
            if(DB::table('meeting_quarentation')->where('quarentation_id',$request->quarentation_id)->delete() && $quarentation->delete()){
                return apiResponse(true,'Data Successfully Deleted');
            }
        }
        return apiResponse(false,'Data Not Found OR Failed to Delete');
    }
}
