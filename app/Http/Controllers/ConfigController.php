<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ConfigController extends Controller
{
    public function configList($value = null)
    {
        $configs = config('arrays');
        if(!empty($value)){
            if(array_key_exists($value, $configs)){
                $string = ucwords(str_replace("_"," ",$value));
                return apiResponse(true, $string.' list' ,array_values(config('arrays.' . $value)));
            }
        }
        return apiResponse(true, 'All Config List' ,config('arrays'));      
    }

    public function index($value = null)
    {
        $configs = config('arrays');
        if(!empty($value)){
            if(array_key_exists($value, $configs)){
                $string = ucwords(str_replace("_"," ",$value));
                return apiResponse(true, $string.' list' ,config('arrays.' . $value));
            }
        }
        return apiResponse(true, 'All Config List' ,$configs);      
    }

    public function faqCategories(){
        return apiResponse(true,'FAQ Category List',config('arrays.faq_categories'));
    }

    public function affectType(){
        return apiResponse(true,'Affect Type List',config('arrays.affect_type'));
    }
}
