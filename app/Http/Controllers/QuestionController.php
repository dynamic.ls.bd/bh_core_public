<?php

namespace App\Http\Controllers;

use App\Feedback;
use App\Http\Resources\QuestionResource;
use App\Http\Resources\QuestionWithFeedback;
use App\Question;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $questions = Question::paginate();
        return apiResponseWithPagination(true,'Question list',QuestionResource::collection($questions),$questions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'to' => 'required',
            'subject' => 'required',
            'message' => 'required'
        ]);

        $created = Question::create([
           'to' => $request->to,
           'subject' => $request->subject,
           'message' => $request->message,
           'created_by' => request()->user()->id
        ]);

        if ($created){
            return apiResponse(true,'Question Successfully Stored');
        }
        return apiResponse(false,'Question Failed to Store');

    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $question = Question::with('feedbacks')->findOrFail($id);
        if(!empty($question))
            return apiResponse(true,'Question with Feedback',new QuestionWithFeedback($question));
        return apiResponse(true,'Data Not Found');

    }

    public function commentQuestion(Request $request, $id)
    {
        $this->validate($request,[
           'feedback' => 'required'
        ]);

        $question = Question::find($id);

        $feedback = new Feedback();
        $feedback->user_id = request()->user()->id;
        $feedback->feedback = $request->feedback;

        if ( $question->feedbacks()->save($feedback)){
            return response()->json(['data' => 'Success'], 201);
        }

        return response()->json(['data' => 'fail to created'], 404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function myQuestion()
    {
        $questions = Question::where('created_by', request()->user()->id)->paginate();
        if($questions->isNotEmpty())
            return apiResponseWithPagination(true,'My Question List',QuestionWithFeedback::collection($questions),$questions);
        return apiResponse(true,'No Data Found');
    }
}
