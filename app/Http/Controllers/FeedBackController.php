<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Contribution;
use App\Http\Resources\CommentResource;
use App\Http\Resources\ContributionResource;
use App\Http\Resources\QuestionResource;
use App\Http\Resources\SuggestionResource;
use App\Http\Resources\AllMargedResource;
use App\Question;
use App\Suggestion;
use Illuminate\Http\Request;

class FeedBackController extends Controller
{
    public function getAllData(Request $request){
        $this->validate($request,['load' => 'required|numeric']);
        $take = 5 * $request->load;
        $question = Question::latest()->take($take)->get();
        $comments = Comment::latest()->take($take)->get();
        $suggestion = Suggestion::latest()->take($take)->get();
        $contribution = Contribution::latest()->take($take)->get();
        $allItems = new \Illuminate\Database\Eloquent\Collection;
        $allItems = $question->concat($comments)
                    ->concat($suggestion)
                    ->concat($contribution);
        if($allItems->isNotEmpty())
            return apiResponse(true,'All Data',AllMargedResource::collection($allItems));
        return apiResponse(true,'No Data Found');   
    }

    public function index(Request $request)
    {
        $this->validate($request,[
            'category' => 'required|string'
        ]);
        if($request->category === 'comment'){
            $comments = Comment::paginate();
            if($comments->isNotEmpty())
                return apiResponseWithPagination(true,'Comments List',CommentResource::collection($comments),$comments);
            return apiResponse(true,'No Data Found');
        }elseif($request->category === 'question'){
            $questions = Question::paginate();
            if($questions->isNotEmpty())
                return apiResponseWithPagination(true,'Question list',QuestionResource::collection($questions),$questions);
            return apiResponse(true,'No Data Found');
        }elseif($request->category === 'suggestion'){
            $suggestion = Suggestion::paginate();
            if($suggestion->isNotEmpty())
                return apiResponseWithPagination(true,'Suggestion list',SuggestionResource::collection($suggestion),$suggestion);
            return apiResponse(true,'No Data Found');
        }elseif($request->category === 'contribution'){
            $contribution = Contribution::paginate();
            if($contribution->isNotEmpty())
                return apiResponseWithPagination(true,'Contribution List',ContributionResource::collection($contribution),$contribution);
            return apiResponse(true,'No Data Found');
        }else{
            return apiResponse(false,'Category not Found');
        }
    }

    public function getMyFeedBacks(Request $request)
    {
        $this->validate($request,[
            'category' => 'required|string'
        ]);
        $user = request()->user();
        if($request->category === 'comment'){
            $comments = Comment::where('created_by','=',$user->id)->paginate();
            if($comments->isNotEmpty())
                return apiResponseWithPagination(true,'My Comments List',CommentResource::collection($comments),$comments);
            return apiResponse(true,'No Data Found');
        }elseif($request->category === 'question'){
            $questions = Question::where('created_by','=',$user->id)->paginate();
            if($questions->isNotEmpty())
                return apiResponseWithPagination(true,'My Question list',QuestionResource::collection($questions),$questions);
            return apiResponse(true,'No Data Found');
        }elseif($request->category === 'suggestion'){
            $suggestion = Suggestion::where('created_by','=',$user->id)->paginate();
            if($suggestion->isNotEmpty())
                return apiResponseWithPagination(true,'My Suggestion list',SuggestionResource::collection($suggestion),$suggestion);
            return apiResponse(true,'No Data Found');
        }elseif($request->category === 'contribution'){
            $contribution = Contribution::where('created_by','=',$user->id)->paginate();
            if($contribution->isNotEmpty())
                return apiResponseWithPagination(true,'My Contribution List',ContributionResource::collection($contribution),$contribution);
            return apiResponse(true,'No Data Found');
        }else{
            return apiResponse(false,'Category not Found');
        }
    }

    public function store(Request $request){
        $this->validate($request,[
            'category' => 'required|string',
            'to' => 'required|string',
            'subject' => 'required|string',
            'message' => 'required|string'
        ]);
        switch ($request->category){
            case 'comment':
                $comment = new Comment();
                return $this->storeFeedBack($comment,$request);
                break;
            case 'question':
                $question = new Question();
                return $this->storeFeedBack($question,$request);
                break;
            case 'suggestion':
                $suggestion = new Suggestion();
                return $this->storeFeedBack($suggestion,$request);
                break;
            case 'contribution':
                $contribution = new Contribution();
                return $this->storeFeedBack($contribution,$request);
                break;
            default:
                return apiResponse(false,'Category not Found');
                break;
        }
    }

    public function editMyFeedBack(Request $request){
        $this->validate($request,[
            'category' => 'required|string',
            'feedback_id' => 'required|numeric',
        ]);

        $user = request()->user();
        if($request->category === 'comment'){
            $comments = Comment::where('created_by','=',$user->id)->where('id','=',$request->feedback_id)->first();
            if(!empty($comments))
                return apiResponse(true,'My Comments List',new CommentResource($comments));
            return apiResponse(true,'No Data Found');
        }elseif($request->category === 'question'){
            $questions = Question::where('created_by','=',$user->id)->where('id','=',$request->feedback_id)->first();
            if(!empty($questions))
                return apiResponse(true,'My Question list',new QuestionResource($questions));
            return apiResponse(true,'No Data Found');
        }elseif($request->category === 'suggestion'){
            $suggestion = Suggestion::where('created_by','=',$user->id)->where('id','=',$request->feedback_id)->first();
            if(!empty($suggestion))
                return apiResponse(true,'My Suggestion list',new SuggestionResource($suggestion));
            return apiResponse(true,'No Data Found');
        }elseif($request->category === 'contribution'){
            $contribution = Contribution::where('created_by','=',$user->id)->where('id','=',$request->feedback_id)->first();
            if(!empty($contribution))
                return apiResponse(true,'My Contribution List',new ContributionResource($contribution));
            return apiResponse(true,'No Data Found');
        }else{
            return apiResponse(false,'Category not Found');
        }
    }

    public function update(Request $request){
        $this->validate($request,[
            'category' => 'required|string',
            'feedback_id' => 'required|numeric',
            'to' => 'required|string',
            'subject' => 'required|string',
            'message' => 'required|string'
        ]);
        $user = request()->user();
        switch ($request->category){
            case 'comment':
                $comment = Comment::where('created_by','=',$user->id)->where('id','=',$request->feedback_id)->first();
                if(!empty($comment))
                    return $this->updateFeedBack($comment,$request);
                return apiResponse(true,'No Data Found');
                break;
            case 'question':
                $question = Question::where('created_by','=',$user->id)->where('id','=',$request->feedback_id)->first();
                if(!empty($question))
                    return $this->updateFeedBack($question,$request);
                return apiResponse(true,'No Data Found');
                break;
            case 'suggestion':
                $suggestion = Suggestion::where('created_by','=',$user->id)->where('id','=',$request->feedback_id)->first();
                if(!empty($suggestion))
                    return $this->updateFeedBack($suggestion,$request);
                return apiResponse(true,'No Data Found');
                break;
            case 'contribution':
                $contribution = Contribution::where('created_by','=',$user->id)->where('id','=',$request->feedback_id)->first();
                if(!empty($contribution))
                    return $this->updateFeedBack($contribution,$request);
                return apiResponse(true,'No Data Found');
                break;
            default:
                return apiResponse(false,'Category not Found');
                break;
        }
    }

    private function storeFeedBack($model,$request){
        $model->to = $request->to;
        $model->subject = $request->subject;
        $model->message = $request->message;
        $model->created_by = request()->user()->id;
        if($model->save())
            return apiResponse(true,'Successfully Created');
        return apiResponse(false,'Failed to create');
    }

    private function updateFeedBack($model,$request){
        $model->to = $request->to;
        $model->subject = $request->subject;
        $model->message = $request->message;
        $model->created_by = request()->user()->id;
        if($model->save())
            return apiResponse(true,'Successfully Updated');
        return apiResponse(false,'Failed to Updated');
    }
}
