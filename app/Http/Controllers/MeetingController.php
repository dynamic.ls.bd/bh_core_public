<?php

namespace App\Http\Controllers;

use App\Http\Resources\MeetingResource;
use App\Quarentetion;
use App\User;
use App\Meeting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MeetingController extends Controller
{
    public function index()
    {
        $meetings = Meeting::where('with_whom','!=',null)->paginate();
        if($meetings->isNotEmpty())
            return apiResponseWithPagination(true,'Meeting List',MeetingResource::collection($meetings),$meetings);
        return apiResponse(true, 'No Data Found');
    }

    public function myMeetings()
    {
        $meetings = Meeting::where('with_whom','!=',null)->where('me','=',request()->user()->id)->paginate();
        if($meetings->isNotEmpty())
            return apiResponseWithPagination(true,'My Meeting List',MeetingResource::collection($meetings),$meetings);
        return apiResponse(true, 'No Data Found');
    }

    public function myTodaysMeetings()
    {
        $meetings = Meeting::where('with_whom','!=',null)
        ->where('me','=',request()->user()->id)
        ->whereDate('date_time','=',Carbon::today())
        ->paginate();
        if($meetings->isNotEmpty())
            return apiResponseWithPagination(true,'My Todays Meeting List',MeetingResource::collection($meetings),$meetings);
        return apiResponse(true, 'No Data Found');
    }

    public function store(Request $request){
        $this->validate($request,[
            'user_id' => 'nullable|integer',
            'name' => 'nullable|string',
            'date_time' => 'nullable|string',
            'phone' => 'nullable|digits:11',
            'type' => 'nullable|digits:1',
            'lat' => 'nullable|numeric',
            'lng' => 'nullable|numeric',
        ]);

        $date = new Carbon($request->date_time);
        $current = Carbon::now();
        if($date->gt($current))
            return apiResponse(false, 'Choosen Date Must Be Before Current Date');

        DB::beginTransaction();
        try{
            $flag = 1;
            $isUserExists = User::where('name','=',$request->name)->orWhere('phone','=',$request->phone)->first();
            if(!empty($isUserExists)){
                $flag = 0;
            }
            $meeting = new Meeting();
            $meeting->me = request()->user()->id;
            $meeting->with_whom = $request->user_id ?? $isUserExists ? $isUserExists->id : $isUserExists;
            $meeting->type = $request->type;
            $meeting->lat = $request->lat;
            $meeting->lng = $request->lng;
            $meeting->date_time = $request->date_time;
            if($meeting->save()){
                $quarentation = Quarentetion::where('user_id','=',request()->user()->id)
                    ->whereDate('created_at', '=', Carbon::today()->toDateString())->pluck('id');
                if($quarentation->isNotEmpty()){
                    $meeting->quarentation()->sync($quarentation);
                }
                if(empty($request->user_id) && $flag){
                    $user = new User();
                    $user->name = $request->name;
                    $user->phone = $request->phone;
                    $user->static = User::NONREGISTERED;
                    $user->save();
                    $meeting->with_whom = $user->id;
                    $meeting->save();
                }
            }
            DB::commit();
            return apiResponse(true,'Data stored');
        }catch (\Exception $exception){
            DB::rollBack();
            return apiResponse(false,'Data Store Failed');
        }
    }

    public function edit($id){
        $meeting = Meeting::find($id);
        if(!empty($meeting)){
            return apiResponse(true,'Meeting Data',new MeetingResource($meeting));
        }
        return apiResponse(true,'No Data Found');
    }

    public function deleteMeetingUser(Request $request){
        $meeting = Meeting::find($request->meeting_id);
        if(!empty($meeting)){
            $meeting->with_whom = null;
            if($meeting->save())
                return apiResponse(true,'Meeting User Deleted',new MeetingResource($meeting));
        }
        return apiResponse(true,'Data Not Found');
    }

    public function searchUser(Request $request){
        $users = User::where('name','like','%'.$request->term.'%')->orWhere('phone','like','%'.$request->term.'%')->get();
        if($users->isNotEmpty()){
            return apiResponse(true,'User List',\App\Http\Resources\User::collection($users));
        }
        return apiResponse(true,'No Data Found');
    }

    public function update(Request $request){
        $this->validate($request,[
            'meeting_id' => 'required|integer',
            'user_id' => 'nullable|integer',
            'name' => 'nullable|string',
            'date_time' => 'nullable|string',
            'phone' => 'nullable|digits:11',
            'type' => 'nullable|digits:1',
            'lat' => 'nullable|numeric',
            'lng' => 'nullable|numeric',
        ]);
        DB::beginTransaction();
        try{
            $flag = 1;
            $isUserExists = User::where('name','=',$request->name)->orWhere('phone','=',$request->phone)->first();
            if(!empty($isUserExists)){
                $flag = 0;
            }
            $meeting = Meeting::find($request->meeting_id);
            $meeting->me = request()->user()->id;
            $meeting->with_whom = $request->user_id ?? $isUserExists ? $isUserExists->id : $isUserExists;
            $meeting->type = $request->type;
            $meeting->lat = $request->lat;
            $meeting->lng = $request->lng;
            $meeting->date_time = $request->date_time;
            if($meeting->save()){
                if(empty($request->user_id) && $flag){
                    $user = new User();
                    $user->name = $request->name;
                    $user->phone = $request->phone;
                    $user->static = User::NONREGISTERED;
                    $user->save();
                    $meeting->with_whom = $user->id;
                    $meeting->save();
                }
            }
            DB::commit();
            return apiResponse(true,'Data Updated',new MeetingResource($meeting));
        }catch (\Exception $exception){
            DB::rollBack();
            return apiResponse(false,'Data Update Failed');
        }
    }

    public function destroy(Request $request){
        $meeting = Meeting::find($request->meeting_id);
        if(!empty($meeting)){
//            if(DB::table('meeting_quarentation')->where('meeting_id','=',$request->meeting_id)->delete() && $meeting->delete())
//                return apiResponse(true,'Meeting Deleted');
            DB::table('meeting_quarentation')->where('meeting_id','=',$request->meeting_id)->delete();
            $meeting->delete();

            return apiResponse(true,'Meeting Deleted');
        }
        return apiResponse(true,'Failed To Delete');
    }
}
