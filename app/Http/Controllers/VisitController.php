<?php

namespace App\Http\Controllers;

use App\Http\Resources\VisitResource;
use App\Visit;
use Illuminate\Http\Request;
use Carbon\Carbon;

class VisitController extends Controller
{
    public function index(){
        $visits = Visit::with('user')->paginate();
        if($visits->isNotEmpty())
            return apiResponseWithPagination(true,'All Visit List',VisitResource::collection($visits),$visits);
        return apiResponse(true,'Data Not Found');
    }

    public function myVisits(){
        $visits = Visit::where('user_id','=',request()->user()->id)->paginate();
        if($visits->isNotEmpty())
            return apiResponseWithPagination(true,'My Visit List',VisitResource::collection($visits),$visits);
        return apiResponse(true,'Data Not Found');
    }

    public function myTodaysVisit(){
        $visits = Visit::where('user_id','=',request()->user()->id)->whereDate('date_time','=',Carbon::today())->paginate();
        if($visits->isNotEmpty())
            return apiResponseWithPagination(true,'My Todays Visit List',VisitResource::collection($visits),$visits);
        return apiResponse(true,'Data Not Found');
    }

    public function store(Request $request){
        $this->validate($request,[
            'address' => 'required|string',
            'lat' => 'nullable|numeric',
            'lng' => 'nullable|numeric',
            'date_time' => 'required|string',
        ]);

        $date = new Carbon($request->date_time);
        $current = Carbon::now();
        if($date->gt($current))
            return apiResponse(false, 'Choosen Date Must Be Before Current Date');

        $visit = new Visit();
        $visit->user_id = request()->user()->id;
        $visit->address = $request->address;
        $visit->lat = $request->lat;
        $visit->lng = $request->lng;
        $visit->date_time = $request->date_time;
        if($visit->save())
            return apiResponse(true,'Data Successfully Stored');
        return apiResponse(false,'Failed to Store');
    }

    public function edit($id){
        $visit = Visit::find($id);
        if(!empty($visit))
            return apiResponse(true,'Edit Visit Data',$visit);
        return apiResponse(true,'Data Not Found');
    }

    public function update(Request $request){
        $this->validate($request,[
            'visit_id' => 'required|numeric',
            'address' => 'required|string',
            'lat' => 'nullable|numeric',
            'lng' => 'nullable|numeric',
            'date_time' => 'required',
        ]);
        $visit = Visit::find($request->visit_id);
        if(!empty($visit)){
            $visit->user_id = request()->user()->id;
            $visit->address = $request->address;
            $visit->lat = $request->lat;
            $visit->lng = $request->lng;
            $visit->date_time = $request->date_time;
            if($visit->save())
                return apiResponse(true,'Data Successfully Updated');
            return apiResponse(false,'Failed to Update');
        }
        return apiResponse(true,'Data Not Found');
    }

    public function destroy(Request $request){
        $this->validate($request,[
            'visit_id' => 'required|numeric'
        ]);
        $visit = Visit::find($request->visit_id);
        if(!empty($visit))
            if($visit->delete())
                return apiResponse(true,'Data Successfully Deleted');
            else
                return apiResponse(false,'Failed to Delete');
        return apiResponse(true,'Data Not Found');
    }
}
