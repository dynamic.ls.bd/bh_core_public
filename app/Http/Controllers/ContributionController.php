<?php

namespace App\Http\Controllers;

use App\Contribution;
use App\Feedback;
use App\Http\Resources\ContributionResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Validation\ValidationException;

class ContributionController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index()
    {
        $contribution = Contribution::paginate();
        if($contribution->isNotEmpty())
            return apiResponseWithPagination(true,'Contribution List',ContributionResource::collection($contribution),$contribution);
        return apiResponse(true,'No Data Found');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'to' => 'required',
            'subject' => 'required',
            'message' => 'required',
        ]);

        $created = Contribution::create([
            'to' => $request->to,
            'subject' => $request->subject,
            'message' => $request->message,
            'created_by' => request()->user()->id
        ]);

        if ($created){
            return apiResponse(true,'Contribution Successfully Created');
        }
        return apiResponse(false,'Failed to Create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contribution  $contribution
     * @return JsonResponse
     */
    public function show($id)
    {
        $contribution = Contribution::with('feedbacks')->findOrFail($id);
        if(!empty($contribution))
            return apiResponse(true,'Contribution',new ContributionResource($contribution));
        return apiResponse(true,'No Data Found');
    }

    public function comment(Request $request, $id)
    {
        $this->validate($request,[
            'feedback' => 'required'
        ]);

        $suggestion = Contribution::find($id);

        $save = new Feedback();
        $save->user_id = request()->user()->id;
        $save->feedback = $request->feedback;

        if ($suggestion->feedbacks()->save($save)){
            return apiResponse(true,'Comment Successfully Created');
        }

        return apiResponse(false,'Failed to Create');
    }

    public function myContribution()
    {
        $myContribution = Contribution::where('created_by', request()->user()->id)->paginate();
        if($myContribution->isNotEmpty())
            return apiResponseWithPagination(true,'My Contribution List',ContributionResource::collection($myContribution),$myContribution);
        return apiResponse(true,'No Data Found');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contribution  $contribution
     * @return \Illuminate\Http\Response
     */
    public function edit(Contribution $contribution)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  \App\Contribution  $contribution
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contribution $contribution)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contribution  $contribution
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contribution $contribution)
    {
        //
    }
}
