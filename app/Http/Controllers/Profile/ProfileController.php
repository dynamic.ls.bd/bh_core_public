<?php

namespace App\Http\Controllers\Profile;

use App\Http\Resources\ProfileResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Profile\UpdateProfileRequest;
use App\Http\Requests\Profile\UpdatePassword;
use App\Http\Resources\User as UserResource;
use Illuminate\Support\Facades\Hash;
use App\Profile;

class ProfileController extends Controller
{
     /**
     * Get Login User
     *
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me(Request $request)
    {
        // Get data of Logged user
        $user = Auth::user();
        if(!$user->profile()->exists()){
            Profile::create([
                'user_id' => $user->id
            ]);
        }
        return apiResponse(true,'Your Profile',new ProfileResource($user));
    }


     /**
     * Update Profile
     *
     *
     * @param UpdateProfileRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateProfileRequest $request)
    {
        // Get data of Logged user
        $user =  request()->user();

        $profile = $user->profile;
        // Update User
        $user->update($request->only('name','email','phone'));
        // Update profile
        $profile->user_id = $user->id;
        $profile->dob = $request->dob;
        if($request->hasFile('image'))
        {
            $profile->image = storeThumb($request->image);
        }
        $profile->address = $request->address;
        $profile->job_address = $request->job_address;
        $profile->occupation = $request->occupation;
        $profile->lat = $request->lat;
        $profile->lng = $request->lng;
        $profile->age = $request->age;
        $profile->gender = $request->gender ?? 1;
        $profile->blood_group = $request->blood_group;
        $profile->diabetes = $request->diabetes;
        $profile->heart_disease = $request->heart_disease;
        $profile->kidney_disease = $request->kidney_disease;
        $profile->respiratory_disease = $request->respiratory_disease;
        $profile->others = $request->others;
        $profile->upazila_id = $request->upazila_id;
        $profile->returned_from_abroad = $request->returned_from_abroad;
        $profile->rb_country_id = $request->rb_country_id;
        $profile->rba_date = $request->rba_date;
        $profile->save();

        return apiResponse(true,'Your Profile',new ProfileResource($user));

    }

     /**
     * Update Profile
     *
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePassword(UpdatePassword $request)
    {
        // Get Request User
        $user = $request->user();

        // Validate user Password and Request password
        if (!Hash::check($request->current_password, $user->password)) {
            // Validation failed return an error messsage
            return response()->json(['error' => 'Invalid current password'], 422);

        }

        // Update User password
        $user->update([
            'password' =>  Hash::make($request->new_password),
        ]);

        // transform user data
        $data = new UserResource($user);

        return response()->json(compact('data'));
    }
}
