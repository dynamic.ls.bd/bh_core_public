<?php

namespace App\Http\Controllers;

use App\Feedback;
use App\Http\Resources\SuggestionResource;
use App\Http\Resources\SuggestionWithFeedback;
use App\Suggestion;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class SuggestionController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index()
    {
        $suggestion = Suggestion::paginate();
        if($suggestion->isNotEmpty())
            return apiResponseWithPagination(true,'Suggestion list',SuggestionResource::collection($suggestion),$suggestion);
        return apiResponse(true,'No Data Found');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'to' => 'required',
            'subject' => 'required',
            'message' => 'required',
        ]);

        $created = Suggestion::create([
            'to' => $request->to,
            'subject' => $request->subject,
            'message' => $request->message,
            'created_by' => request()->user()->id
        ]);

        if ($created){
            return apiResponse(true,'Successfully Created');
        }

        return apiResponse(true,'Failed to Create');
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $question = Suggestion::with('feedbacks')->find($id);

        return apiResponse(true,'Suggestion',new SuggestionWithFeedback($question));
    }

    public function mySuggestion()
    {
        $suggestion = Suggestion::where('created_by', request()->user()->id)->paginate();
        if($suggestion->isNotEmpty())
            return apiResponseWithPagination(true,'My Suggestion List',SuggestionResource::collection($suggestion),$suggestion);
        return apiResponse(true,'No Data Found');
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function commentSuggestion(Request $request, $id)
    {
        $this->validate($request,[
            'feedback' => 'required'
        ]);

        $suggestion = Suggestion::find($id);

        $save = new Feedback();
        $save->user_id = request()->user()->id;
        $save->feedback = $request->feedback;

        if ($suggestion->feedbacks()->save($save)){
            return apiResponse(true,'Data Successfully Stored');
        }

        return apiResponse(true,'Failed to Create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Suggestion  $suggestion
     * @return \Illuminate\Http\Response
     */
    public function edit(Suggestion $suggestion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  \App\Suggestion  $suggestion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Suggestion $suggestion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Suggestion  $suggestion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Suggestion $suggestion)
    {
        //
    }
}
