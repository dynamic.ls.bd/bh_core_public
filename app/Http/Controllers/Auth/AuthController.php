<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\User;
use Carbon\Carbon;
use Illuminate\Hashing\BcryptHasher;
use App\Http\Resources\User as UserResource;

class AuthController extends Controller
{
    /**
     * Login
     *
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validator($request);
        // Get User by email or phone
        $user = User::where('phone', $request->email)->orWhere('email','=',$request->email)->first();

        // Return error message if user not found.
        if(!$user) return response()->json(['error' => 'User not found.'], 404);

        // Account Validation
        if (!(new BcryptHasher)->check($request->input('password'), $user->password)) {
            // Return Error message if password is incorrect
            return response()->json(['error' => 'Email or password is incorrect. Authentication failed.'], 401);
        }

        // Get email and password from Request
        $credentials = $this->credentials($request);

        try {
            // Login Attempt
            if (! $token = auth()->attempt($credentials)) {
                // Return error message if validation failed
                return response()->json(['error' => 'Invalid credentials'], 401);

            }
        } catch (JWTException $e) {
            // Return Error message if cannot create token.
            return response()->json(['error' => 'could_not_create_token'], 500);

        }
        $user->firebase_token = $request->firebase_token;
        $user->save();
        // transform user data
        $data = new UserResource($user);

        return response()->json(compact('token', 'data'));
    }

    protected function credentials($request)
    {
        if(is_numeric($request->get('email'))){
            return ['phone'=>$request->get('email'),'password'=>$request->get('password')];
        }
        return ['email' => $request->get('email'), 'password'=>$request->get('password')];
    }

    protected function validator(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|'.$this->getRule($request->email),
            'password' => 'required|string|min:6',
            'firebase_token' => 'required|string'
        ],[
            'email.required' => 'The email is required.',
            'email.email' => 'The email needs to have a valid format.',
            'email.digits' => 'The phone must be 11 digits.'
        ]);
    }
    private function getRule($value){
        if(is_numeric($value)){
            return 'digits:11';
        }
        return 'email';
    }
}
