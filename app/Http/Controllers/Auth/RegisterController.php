<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\User as UserResource;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    use RegistersUsers;

    private function getRule($request)
    {
        $isUserExists = User::where('phone','=', $request->phone)->orWhere('email','=',$request->email)->first();
        if(!empty($isUserExists) && (($isUserExists->status == User::INACTIVE) && ($isUserExists->static == User::NONREGISTERED))){
            return 'unique:users,phone,'.$isUserExists->id;

        }
        return 'unique:users';
    }

    public function register(Request $request)
    {
        
        $this->validate($request,[
            'name' => 'required|string',
            'firebase_token' => 'required|string',
            'email' => 'required|email|unique:users',
            'phone' => 'required|digits:11|'.$this->getRule($request),
            'password' => 'required|string|min:8|confirmed',
            'address' => 'required|string',
            'age' => 'nullable|numeric',
            'role' => 'required|string',
            'occupation' => 'nullable|string',
            'upazila_id' => 'nullable|numeric',
            'returned_from_abroad' => 'nullable|boolean',
            'rba_date' => 'nullable|string',
            'rb_country_id' => 'nullable|numeric',
        ]);

        $isUserExists = User::where('phone','=', $request->phone)->orWhere('email','=',$request->email)->first();
        if(!empty($isUserExists) && (($isUserExists->status == User::INACTIVE) && ($isUserExists->static == User::NONREGISTERED))){
            $user = $isUserExists;
        }else{
            // new User
            $user = new User();
        }
        
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->firebase_token = $request->firebase_token;
        $user->static = User::REGISTERED;
        $user->status = User::ACTIVE;
        $user->password = Hash::make($request->password);
        $user->save();

        $profile = $user->profile()->exists() ? $user->profile : new \App\Profile();        
        $profile->user_id = $user->id;
        $profile->age = $request->age;
        $profile->occupation = $request->occupation;
        $profile->address = $request->address;
        $profile->upazila_id = $request->upazila_id;
        $profile->returned_from_abroad = $request->returned_from_abroad;
        $profile->rb_country_id = $request->rb_country_id;
        $profile->rba_date = $request->rba_date;
        $profile->role = $request->role;
        $profile->save();

        //  Generate token
        $token = auth()->fromUser($user);

        // Transform user data
        $data = new UserResource($user);

        return response()->json(compact('token', 'data'));

    }
}
