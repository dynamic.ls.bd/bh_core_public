<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;
use App\Http\Resources\News as NewsResource;
use App\Http\Resources\NewsListResource;
use App\User;
use App\Notifications\GeneralNotification;
use Illuminate\Support\Facades\Notification;

class NewsController extends Controller
{

    public function index()
    {
        $data = News::orderBy('id','desc')->paginate();
        if($data->isNotEmpty())
            return apiResponseWithPagination(true,'News List',NewsListResource::collection($data),$data);
        return apiResponse(true,'No Data Found');
    }


    public function getCategoryWiseNews(Request $request)
    {
        $this->validate($request,[
            'category' => 'required|numeric'
        ]);
        $news = News::where('type','=',$request->category)->paginate();
        if($news->isNotEmpty())
            return apiResponseWithPagination(true,'News List',NewsListResource::collection($news),$news);
        return apiResponse(true,'No Data Found');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|string',
            'slug' => 'required|string',
            'content' => 'required|string',
            'source' => 'nullable|string',
            'image' => 'required|image|max:1024',
            'type' => 'required',
        ]);

        $news = new News();
        $news->title = $request->title;
        $news->slug = $request->slug;
        $news->content = $request->content;
        $news->source = $request->source;
        $news->type = $request->type;
        $news->user_id = request()->user()->id;
        if($request->hasFile('image'))
            $news->image = storeImage($request->file('image'), 'news');
        if($news->save()){
            $users = User::all();
            foreach ($users as $key => $user) {
                $news->notify((new GeneralNotification($user,$news))->delay(now()->addSecond(10)));
            }
            return apiResponse(true, 'Successfully Stored');
        }
        return apiResponse(false, 'Failed to Create');
    }

    public function show($id)
    {
        $news = News::find($id);
        if(!empty($news))
            return apiResponse(true,'News Details',new NewsResource($news));
        return apiResponse(true,'No Data Found');
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
