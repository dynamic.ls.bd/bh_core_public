<?php

namespace App\Http\Controllers;

use App\Announcement;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Resources\Announcement as AnnouncementResource;
use App\Http\Resources\AnnouncementListResource;

class AnnouncementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $today = Carbon::now()->toDateTimeString();

        $data = Announcement::where(function ($query) use($today) {
            $query->where('published_date', '<', $today)
                ->orWhereNull('published_date');
            }
        )
        ->where(function ($query) use($today) {
            $query->where('unpublished_date', '>', $today)
                ->orWhereNull('unpublished_date');
            }
        )
        ->orderBy('id','desc')
        ->paginate(20);

        if($data->isNotEmpty())
            return apiResponseWithPagination(true,'Announcement List',AnnouncementListResource::collection($data),$data);

        return apiResponse(true,'No Data Found');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $announcement = Announcement::find($id);

        if(!empty($announcement))
            return apiResponse(true,'Announcement Details',new AnnouncementResource($announcement));
            
        return apiResponse(true,'No Data Found');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function edit(Announcement $announcement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Announcement $announcement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Announcement $announcement)
    {
        //
    }
}
