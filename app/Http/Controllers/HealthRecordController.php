<?php

namespace App\Http\Controllers;

use App\HealthRecord;
use App\Repositories\Contracts\HealthRecordRepository;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Resources\HealthRecordResource;

class HealthRecordController extends Controller
{
    /**
     * @var
     */
    protected $health_records;

    public function __construct(HealthRecordRepository $health_records)
    {
        $this->health_records = $health_records;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $record = $this->health_records->getAll()->get();
        if ($record->isNotEmpty()){
            return apiResponse(true,'Health Record List',HealthRecordResource::collection($record));
        }
        return apiResponse(true,'No Data Found');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $user = request()->user();

        $this->validate($request, [
            'date' => 'required'
        ]);

        $request->request->add(['user_id' => $user->id]);

        $this->health_records->store($request->input());

        return apiResponse(true,'Health Record created successfully');
    }

    /**
     * @param HealthRecord $healthRecord
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(HealthRecord $healthRecord)
    {
        if(!empty($healthRecord))
            return apiResponse(true,'Health Record',new HealthRecordResource($healthRecord));
        return apiResponse(true,'No Data Found');
    }

    public function ownRecords(Request $request)
    {
        $user = JWTAuth::user();
        $record = $this->health_records->getOwn($user->id);
        if(!empty($request->date)){
            $record = $record->where('date', '=', $request->date);
        }
        $record = $record->get();
        if ($record->isNotEmpty()){
            return apiResponse(true,'Health Record',$record);
        }
        return apiResponse(true,'No Data Found');
    }

    /**
     * @param HealthRecord $healthRecord
     * @param Request $request
     */
    public function edit(HealthRecord $healthRecord, Request $request)
    {
        //
    }

    /**
     * @param Request $request
     * @param HealthRecord $healthRecord
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, HealthRecord $healthRecord)
    {
        $this->health_records->update($healthRecord, $request->input());
        return apiResponse(true,'Health Record updated successfully');
    }

    /**
     * @param HealthRecord $healthRecord
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $healthRecord = HealthRecord::find($id);
        if(!empty($healthRecord))
            if($healthRecord->delete())
                return apiResponse(true,'Health Record deleted successfully');
        return apiResponse(true,'No Data Found Or Failed to delete');
    }
}
