<?php

namespace App\Http\Controllers;

use App\Circle;
use App\CircleUser;
use App\Http\Resources\CircleUserResource;
use App\Http\Resources\CircleEditUser;
use App\Profile;
use App\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class CircleUserController extends Controller
{
    public function index()
    {
        //
    }

    public function swapUser(Request $request)
    {
        $this->validate($request,[
           'current_circle_id' => 'required|numeric',
           'target_circle' => 'required|string',
           'user_id' => 'required|numeric',
        ]);

        DB::beginTransaction();
        try{
            $authUser = request()->user();
            $circle = Circle::where('category','=',$request->target_circle)->where('created_by','=',$authUser->id)->first();
            if (empty($circle)) { // if circle not exists
                $circle = Circle::create([
                    'title' => $authUser->name.'_'.$request->target_circle.'_'.time(),
                    'created_by' => $authUser->id,
                    'category' => $request->target_circle
                ]);

                CircleUser::create([
                    'circle_id' => $circle->id,
                    'user_id' => $authUser->id,
                    'isAdmin' => 1
                ]);
            }
            $user = User::find($request->user_id);
            if(!empty($user)){
                $circleUser = Circle::whereHas('users',function($query) use ($request){
                    $query->where('user_id','=',$request->user_id)
                    ->where('circle_id','=',$request->current_circle_id);
                })
                ->where('created_by','=',$authUser->id)
                ->first();

                if(!empty($circleUser)){
                    $circleUser->users()->detach($request->user_id);
                    CircleUser::create([
                        'circle_id' => $circle->id,
                        'user_id' => $user->id,
                    ]);
                }
            }

            DB::commit();
            return apiResponse(true,$user->name.' Switched to '.ucfirst($request->target_circle).' circle');
        }catch (\Exception $e) {
            DB::rollBack();
            return apiResponse(false,'User Not Found OR Failed to Switch');
        }

    }

    public function store(Request $request)
    {
        $this->validate($request,[
           'name' => 'nullable|string',
           'circle' => 'required|string',
           'phone' => 'required|digits:11',
        ]);

        DB::beginTransaction();

        try {
            $authUser = request()->user();
            $circle = Circle::where('category','=',$request->circle)->where('created_by','=',$authUser->id)->first();
            if (empty($circle)) { // if circle not exists
                $circle = Circle::create([
                    'title' => $authUser->name.'_'.$request->circle.'_'.time(),
                    'created_by' => $authUser->id,
                    'category' => $request->circle
                ]);

                CircleUser::create([
                    'circle_id' => $circle->id,
                    'user_id' => $authUser->id,
                    'isAdmin' => 1
                ]);
            }

            $user = User::where('phone','=',$request->phone)->first();

            if(empty($user)){ // if user not exists
                $user = new User();
                $user->name = $request->name;
                $user->phone = $request->phone;
                $user->static = User::NONREGISTERED;
                $user->status = User::INACTIVE;
                $user->save();

                Profile::create([
                    'user_id' => $user->id,
                    'age' => $request->age,
                    'address' => $request->address,
                    'occupation' => $request->occupation,
                    'job_address' => $request->job_address
                ]);
            }

            CircleUser::create([
                'circle_id' => $circle->id,
                'user_id' => $user->id,
                'relation' => $request->relation
            ]);

            DB::commit();
            return apiResponse(true,'Circle user created successfully');

        }catch (\Exception $e) {
            DB::rollBack();
            return apiResponse(false,'User Already Added OR Failed to Created');
        }
    }

    public function show(CircleUser $circleUser)
    {
        //
    }

    public function edit(Request $request)
    {
        $this->validate($request,[
            'circle_id' => 'required|numeric',
            'user_id' => 'required|numeric'
        ]);

        $circle = Circle::with(['users' => function ($query) use ($request){
            $query->where('user_id','=',$request->user_id)->with('profile');
        }])->where('created_by','=',request()->user()->id)
        ->where('id','=',$request->circle_id)->first();
        if(!empty($circle))
            return apiResponse(true,'Edit circle User',new CircleEditUser($circle));
        return apiResponse(true,'No Data Found');
    }

    public function update(Request $request)
    {
        $this->validate($request,[
            'circle_id' => 'required|numeric',
            'user_id' => 'required|numeric',
            'name' => 'required',
            'phone' => 'required|unique:users,phone,'.$request->user_id,
        ]);
        $circle = Circle::with(['users' => function ($query) use ($request){
            $query->where('user_id','=',$request->user_id);
        }])->where('created_by','=',request()->user()->id)
            ->where('id','=',$request->circle_id)->first();

        if(!empty($circle) && $circle->users->isNotEmpty()){
            DB::beginTransaction();
            try {
                $user = $circle->users[0];
                $user->name = $request->name;
                $user->phone = $request->phone;
                $user->static = 0;
                $user->status = 2;
                $user->save();

                $user->profile()->update([
                    'age' => $request->age,
                    'address' => $request->address,
                    'occupation' => $request->occupation,
                    'job_address' => $request->job_address
                ]);

                $pivot = $user->pivot;
                $pivot->relation = $request->relation;
                $pivot->save();

                DB::commit();
                return apiResponse(true,'Circle user Updated successfully');

            }catch (\Exception $e) {
                DB::rollBack();
                return apiResponse(false,'Failed to Update');
            }
        }
        return apiResponse(true,'No Data Found');
    }

    public function destroy(Request $request)
    {
        $this->validate($request,[
            'circle' => 'required|string',
            'user_id' => 'required|numeric'
        ]);
        $authID = request()->user()->id;
        $circle = Circle::where('category','=',$request->circle)->where('created_by','=',$authID)->first();
        if(!empty($circle)){
            if (($authID == $circle->created_by) && ($authID != $request->user_id)){
                $circleUser = CircleUser::where('circle_id','=', $circle->id)->where('user_id','=', $request->user_id)->delete();
                if($circleUser)
                    return apiResponse(true,'Successfully Deleted');
                return apiResponse(false,'Failed to Delete');
            }
            return apiResponse(false,'Admin Can not be deleted');
        }
        return apiResponse(false,'You are Not circle Admin');
    }
}
