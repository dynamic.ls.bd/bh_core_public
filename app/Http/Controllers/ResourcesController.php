<?php

namespace App\Http\Controllers;

use App\Division;
use App\Resource;
use App\Statistic;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class ResourcesController extends Controller
{
    public function index(Request $request)
    {
        $resourceType = $request->resource_type;

        $data = DB::table('divisions')->leftJoin('districts','divisions.id','=','districts.division_id')
            ->leftJoin('upazilas','districts.id', '=', 'upazilas.district_id')
            ->leftJoin('resources','upazilas.id','=','resources.upazila_id')
            ->when($resourceType, function ($q, $resourceType){
                return $q->where('resources.resource_type','=',$resourceType);
            })
            ->select('divisions.*',
                DB::Raw("SUM(resources.resource_available) AS resource_available")
            )
            ->groupBy('id')
            ->get();

        if($data->isNotEmpty())
            return apiResponse(true,'All Statistic', $data);
        return apiResponse(true,'No Data Found');
    }
}
