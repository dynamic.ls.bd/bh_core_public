<?php

namespace App\Http\Controllers;

use App\Upazila;
use App\Country;
use Illuminate\Http\Request;
use App\Http\Resources\UpazilaResource;

class GeoController extends Controller
{
    public function searchUpazila(Request $request)
    {
        $upazilas = Upazila::where('name','like','%'.$request->term.'%')->take(20)->get();

        if($upazilas->isNotEmpty()){
            return apiResponse(true,'Upazila List',UpazilaResource::collection($upazilas));
        }
        return apiResponse(true,'No Data Found');
    }

    public function searchCountry(Request $request){
    	$this->validate($request,[
    		'term' => 'required|string'
    	]);

    	$countries = Country::where('name','like','%'.$request->term.'%')->take(20)->get();
    	if($countries->isNotEmpty())
    		return apiResponse(true,'Country List',$countries);
    	return apiResponse(true,'No Data Found');
    }
}
