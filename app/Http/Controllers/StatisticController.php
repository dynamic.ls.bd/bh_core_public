<?php

namespace App\Http\Controllers;

use App\Division;
use App\Statistic;
use App\Upazila;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class StatisticController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $data = Division::leftJoin('districts','divisions.id','=','districts.division_id')
            ->leftJoin('upazilas','districts.id', '=', 'upazilas.district_id')
            ->leftJoin('statistics','upazilas.id','=','statistics.upazila_id')
            ->select('divisions.*',DB::Raw("SUM(statistics.quarantine_web) AS quarantine_web"),
                DB::Raw("SUM(statistics.quarantine_app) AS quarantine_app"),
                DB::Raw("sum(CASE WHEN (statistics.type = 'infected') THEN statistics.people_number ELSE '' END) AS total_case"),
                DB::Raw("sum(CASE WHEN (statistics.type = 'deceased') THEN statistics.people_number ELSE '' END) AS death"),
                DB::Raw("sum(CASE WHEN (statistics.type = 'recovered') THEN statistics.people_number ELSE '' END) AS recovered"),
                DB::Raw("sum(CASE WHEN (statistics.type = 'sick') THEN statistics.people_number ELSE '' END) AS currently_sick")
            )->groupBy('id')->get();

        if($data->isNotEmpty()){
            $stats_data = [];
            //virus cased
            $stats_data['total_case'] = $data->sum('total_case');
            $stats_data['death'] = $data->sum('death');
            $stats_data['recovered'] = $data->sum('recovered');
            $stats_data['currently_sick'] = $stats_data ['total_case'] - ($stats_data ['death'] + $stats_data ['recovered']);
            $stats_data['today_case'] = Statistic::where('type','!=','recovered')
                                    ->whereDate('date', Carbon::today())->sum('people_number');

            //total quarantine
            $stats_data ['total_quarantine'] = $data->sum('quarantine_web');

            $stats_data['stats_data'] = $data; 

            return apiResponse(true,'All Statistic', $stats_data);
        }
        return apiResponse(true,'No Data Found');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'upazila' => 'required',
            'date' => 'required',
            'type' => 'required',
        ]);

        $upazila = Upazila::find($request->upazila);

        $saved = Statistic::create([
            'upazila_id' => $request->upazila,
            'location' => $request->location,
            'lat' => $upazila->lat,
            'lng' => $upazila->lng,
            'date' => $request->date,
            'type' => $request->type,
            'people_number' => $request->people_number,
            'quarantine_web' => $request->quarantine_web,
            'created_by' => $request->user()->id
        ]);

        if ($saved){
            return apiResponse(true,'Statistics created successfully');
        }
        return apiResponse(false,'Failed to Created');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Statistic  $statistic
     * @return \Illuminate\Http\Response
     */
    public function show(Statistic $statistic)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Statistic  $statistic
     * @return \Illuminate\Http\Response
     */
    public function edit(Statistic $statistic)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Statistic  $statistic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Statistic $statistic)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Statistic  $statistic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Statistic $statistic)
    {
        //
    }

    public function dailyConfirmed()
    {
        $currentMonthNumber = date('m'); //current month number
        $currentMonthName = now()->format('F');// current month name
        //current month data
        $data = Statistic::select('date','quarantine_web')->whereRaw('MONTH(created_at) = ?',[$currentMonthNumber])
            ->orderBy('date','asc')->get();
        //last 30 days data
        //$data = Statistic::select('date','quarantine_web')->whereDate('date', '>', now()->subDays(30))->get();

        $data['currentMonth'] = $currentMonthName;

        if($data->isNotEmpty())
            return apiResponse(true,'Current month daily confirmed data', $data);
        return apiResponse(true,'No Data Found');
    }

    public function dailyCumulative()
    {
        $currentMonthNumber = date('m'); //current month number
        $currentMonthName = now()->format('F');// current month name

        $data = Statistic::select('date','quarantine_app')->whereRaw('MONTH(created_at) = ?',[$currentMonthNumber])
            ->orderBy('date','asc')->get();
        $data['currentMonth'] = $currentMonthName;

        if($data->isNotEmpty())
            return apiResponse(true,'Current month daily cumulative data', $data);
        return apiResponse(true,'No Data Found');
    }
}
