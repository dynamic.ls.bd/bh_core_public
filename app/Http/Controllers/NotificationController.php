<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\NotificationResource;

class NotificationController extends Controller
{
    public function getAllNotification()
    {
    	$user = request()->user();
    	$notifications = config('arrays.default_notification');
    	$notify = $user->notifications->toArray() ?? [];
    	foreach ($notifications as $key => $value) {
    		array_push($notify, $value);
    	}
    	if(!empty($notify))
    		return apiResponse(true,'Your Notifications',NotificationResource::collection($notify));
		return apiResponse(true,'No Data Found');
    }

    public function markAllNotificationsAsRead()
    {
    	$user = request()->user();
    	$user->unreadNotifications->markAsRead();
		return apiResponse(true,'All Unread Notifications are now read');
    }

    public function markNotificationAsRead(Request $request)
    {
    	$this->validate($request,[
    		'id' => 'required|string'
    	]);
    	$user = request()->user();
    	$notification = $user->unreadNotifications->where('id',$request->id)->first();
    	if($notification){
    		$notification->markAsRead();
			return apiResponse(true,'Notification Mark as Read');
    	}
    	return apiResponse(true,'No Data Found');
    }
}
