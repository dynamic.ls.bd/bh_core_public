<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class QuarentationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'day' => $this->day,
            'date' => $this->date ? Carbon::parse($this->date)->format('d F,Y') : null,
            'user_id' => $this->user_id,
            'activity' => $this->activity,
            'sufferings' => $this->sufferings,
            'meetings' => $this->meetings()->exists() ? MeetingResource::collection($this->meetings) : null,
        ];
    }
}
