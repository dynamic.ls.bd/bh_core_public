<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this['id'],
            'title' => $this['data']['title'] ?? $this['title'],
            'message' => $this['data']['message'] ?? $this['message'],
            'reat_at' => $this['read_at'],
        ];
    }
}
