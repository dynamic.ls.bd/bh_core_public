<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ObservationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'to' => $this->tags,
            'tags' => $this->tags,
            'message' => $this->message,
            'user_id' => $this->user_id,
            'created_by' => optional($this->owner)->name,
            'unread' => $this->unread,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
