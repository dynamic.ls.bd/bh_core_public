<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CircleUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'image' => $this->image ? imagePath().$this->image : null,
            'description' => $this->description,
            'category' => $this->category,
            'created_by' => optional($this->createdBy)->name,
            'circle_users' => $this->users()->exists() ? CircleUsers::collection($this->users) : null,
        ];
    }
}
