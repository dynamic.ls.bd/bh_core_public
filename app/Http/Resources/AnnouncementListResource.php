<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class AnnouncementListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'author' => optional($this->author)->name,
            'content' => trimText($this->content, config('arrays.trim_length')),
            'image' => $this->image ? imagePath().$this->image : null,
            'status' => $this->status == 1 ? "Active" : "Inactive",
            'published_date' => !empty($this->published_date) ? Carbon::parse($this->published_date)->format('d F, Y h:i:A') : "",
            'unpublished_date' => !empty($this->unpublished_date) ? Carbon::parse($this->unpublished_date)->format('d F, Y h:i:A') : ""
        ];
    }
}
