<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class QuestionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'to' => $this->to,
            'subject' => $this->subject,
            'message' => $this->message,
            'created_by_id' => $this->created_by,
            'created_by' => optional($this->createdBy)->name,
            'unread' => $this->unread,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
