<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class MeetingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'your_name' => optional($this->user)->name,
            'person_name' => optional($this->whom)->name,
            'person_mobile' => optional($this->whom)->phone,
            'meeting_time' => $this->date_time ? Carbon::parse($this->date_time)->format('d F, Y, h:i A') : Carbon::parse($this->created_at)->format('d F, Y, h:i A'),
        ];
    }
}
