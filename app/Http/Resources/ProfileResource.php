<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ProfileResource extends JsonResource
{
    private function getUpazila($profile){
        if($profile->upazila()->exists()){
            return $profile->upazila->name.' -> '.$profile->upazila->district->name.' -> '.$profile->upazila->district->divisions->name;
        }
        return null;
    }

    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'firebase_token' => $this->firebase_token,
            'dob' => $this->profile->dob ? Carbon::parse($this->profile->dob)->format('d F, Y') : null,
            'image' => $this->profile->image  ? imagePath().$this->profile->image : null,
            'address' => $this->profile->address,
            'blood_group' => $this->profile->blood_group,
            'gender' => $this->profile->gender == 1 ? 'Male' : 'Female',
            'age' => $this->profile->age,
            'role' => $this->profile->role,
            'lat' => '23.0054981',
            'lng' => '90.885216',
            'returned_from_abroad' => $this->profile->returned_from_abroad ? 'Yes' : 'No',
            'upazila_id' => $this->profile->upazila_id,
            'upazila_name' => self::getUpazila($this->profile),
            'country_id' => $this->profile->rb_country_id,
            'country_name' => $this->profile->country()->exists() ? $this->profile->country->name : '',
            'returned_date' => $this->profile->rba_date ? Carbon::parse($this->profile->rba_date)->format('d F, Y') : null,
            'occupation' => $this->profile->occupation,
            'job_address' => $this->profile->job_address,
            'diabetes' => $this->profile->diabetes,
            'heart_disease' => $this->profile->heart_disease,
            'kidney_disease' => $this->profile->kidney_disease,
            'respiratory_disease' => $this->profile->respiratory_disease,
            'others' => $this->profile->others,
        ];
    }
}
