<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ProfileResource;

class CircleEditUser extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'image' => $this->image ? imagePath().$this->image : null,
            'description' => $this->description,
            'category' => $this->category,
            'created_by' => optional($this->createdBy)->name,
            'user_profile' => $this->users,
        ];
    }
}
