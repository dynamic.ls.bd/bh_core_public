<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class VisitResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'user_name' => optional($this->user)->name,
            'user_phone' => optional($this->user)->phone,
            'visit_address' => $this->address,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'date_time' => Carbon::parse($this->date_time)->format('d F, Y, h:i A')
        ];
    }
}
