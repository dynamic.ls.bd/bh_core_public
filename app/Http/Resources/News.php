<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use Carbon\Carbon;

class News extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'author_id' => $this->user_id,
            'author' => optional($this->author)->name,
            'content' => $this->content,
            'image' => $this->image ? imagePath().$this->image : null,
            'source' => $this->source,
            'type' => $this->type == 1 ? "Local" : "International",
            'created_at' => Carbon::parse($this->created_at)->format('d,F Y - h:i:A'),
            'updated_at' => $this->updated_at,
        ];
    }
}
