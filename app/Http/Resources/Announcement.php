<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use Carbon\Carbon;

class Announcement extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'author_id' => $this->user_id,
            'author' => optional($this->author)->name,
            'content' => $this->content,
            'image' => $this->image ? imagePath().$this->image : null,
            'source' => $this->source,
            'status' => $this->status == 1 ? "Active" : "Inactive",
            'published_date' => !empty($this->published_date) ? Carbon::parse($this->published_date)->format('d F, Y h:i:A') : "",
            'unpublished_date' => !empty($this->unpublished_date) ? Carbon::parse($this->unpublished_date)->format('d F, Y h:i:A') : "",
            'created_at' => Carbon::parse($this->created_at)->format('d F, Y h:i:A'),
            'updated_at' => Carbon::parse($this->updated_at)->format('d F, Y h:i:A')
        ];
    }
}
