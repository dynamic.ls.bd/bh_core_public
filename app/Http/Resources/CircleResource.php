<?php

namespace App\Http\Resources;

use App\CircleUser;
use Illuminate\Http\Resources\Json\JsonResource;

class CircleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'image' => $this->image ? imagePath().$this->image : null,
            'description' => $this->description,
            'category' => $this->category,
//            'circle_users' => CircleUserResource::collection($this->circleUser),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
