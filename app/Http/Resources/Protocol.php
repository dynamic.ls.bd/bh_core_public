<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class Protocol extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'author_id' => $this->user_id,
            'author' => optional($this->author)->name,
            'content' => $this->content,
            'document' => $this->document ? imagePath() . $this->document : null,
            'created_at' => Carbon::parse($this->created_at)->format('d F, Y h:i:A'),
            'updated_at' => Carbon::parse($this->updated_at)->format('d F, Y h:i:A')
        ];
    }
}
