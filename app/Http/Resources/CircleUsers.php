<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CircleUsers extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'static' => $this->static,
            'status' => $this->status,
            'status_label' => $this->status_label,
            'relation' => $this->pivot->relation,
            'role' => $this->pivot->isAdmin ? 'Admin' : 'Member',
        ];
    }
}
