<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\User as UserResource;

class HealthRecordResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => $this->user()->exists() ? new UserResource($this->user) : null,
            'diastolic' => $this->diastolic,
            'systolic' => $this->systolic,
            'pulse' => $this->pulse,
            'date' => $this->date,
            'temperature' => $this->temperature,
            'cough' => $this->cough,
            'sneezing' => $this->sneezing,
            'breathing' => $this->breathing,
            'pain' => $this->pain,
            'hand_wash' => $this->hand_wash,
            'medication' => $this->medication,
            'food_diets' => $this->food_diets,
            'sleeping_hour' => $this->sleeping_hour,
            'activities' => $this->activities,
            'notes' => $this->notes,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
