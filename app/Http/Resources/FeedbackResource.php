<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class FeedbackResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'user_id' => $this->user_id,
            'user_name' => optional($this->user)->name,
            'user_phone' => optional($this->user)->phone,
            'feedback' => $this->feedback,
            'created_at' => Carbon::parse($this->created_at)->format('d F,Y - h:i A'),
        ];
    }
}
