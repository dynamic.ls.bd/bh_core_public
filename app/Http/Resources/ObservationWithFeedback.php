<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ObservationWithFeedback extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'to' => $this->to,
            'tags' => $this->tags,
            'message' => $this->message,
            'user_id' => $this->user_id,
            'created_by' => optional($this->owner)->name,
            'unread' => $this->unread,
            'feedback' => FeedbackResource::collection($this->feedbacks()->latest()->take(3)->get()),
            'created_at' => $this->created_at->format('d F, Y h:i:A'),
            'updated_at' => $this->updated_at->format('d F, Y h:i:A'),
        ];
    }
}
