<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StatisticResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'upazila_id' => $this->upazila_id,
            'location' => $this->location,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'date' => $this->date,
            'type' => $this->type,
            'created_by_id' => $this->created_by,
            'created_by' => optional($this->createdBy)->name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
