<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UpazilaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "division_id" => $this->division->id,
            "district_id" => $this->district_id,
            "name" => $this->name.' -> '. optional($this->district)->name. ' -> '. $this->division->name,
            "web_quarentine_count" => $this->statistics(),
            "lat" => $this->lat,
            "lng" => $this->lng,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
