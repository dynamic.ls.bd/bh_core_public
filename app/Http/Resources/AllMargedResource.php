<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class AllMargedResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'to' => $this->to,
            'subject' => $this->subject,
            'message' => $this->message,
            'category' => $this->model_name,
            'created_by_id' => $this->created_by,
            'created_by' => optional($this->createdBy)->name,
            'created_at' => Carbon::parse($this->created_at)->format('d.m.Y, h:i:a'),
            'updated_at' => $this->updated_at,
        ];
    }
}
