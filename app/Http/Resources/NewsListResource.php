<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class NewsListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'author' => optional($this->author)->name,
            'content' => trimText($this->content, config('arrays.trim_length')),
            'image' => $this->image ? imagePath().$this->image : null,
            'type' => $this->type == 1 ? "Local" : "International",
            'created_at' => Carbon::parse($this->created_at)->format('d,F Y - h:i:A'),
        ];
    }
}
