<?php

namespace App\Http\Requests\Auth;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|'.$this->getRule($this['email']),
            'password' => 'required'
        ];
    }

    private function getRule($value){
        if(is_numeric($value)){
            return 'digits:11';
        }
        return 'email';
    }
}
