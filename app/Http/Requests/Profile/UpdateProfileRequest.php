<?php

namespace App\Http\Requests\Profile;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = request()->user();
        return [
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'phone' => 'required|digits:11|unique:users,phone,'.$user->id,
            'age' => 'nullable|numeric',
            'image' => 'nullable|image|max:1024',
            'address' => 'nullable|string',
            'gender' => 'nullable|boolean',
            'blood_group' => 'nullable|string',
            'occupation' => 'nullable|string',
            'job_address' => 'nullable|string',
            'diabetes' => 'nullable|string',
            'heart_disease' => 'nullable|string',
            'kidney_disease' => 'nullable|string',
            'respiratory_disease' => 'nullable|string',
            'others' => 'nullable|string',
            'upazila_id' => 'nullable|numeric',
            'returned_from_abroad' => 'nullable|boolean',
            'rba_date' => 'nullable|string',
            'rb_country_id' => 'nullable|numeric',
        ];
    }
}
