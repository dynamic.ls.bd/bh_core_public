<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contribution extends Model
{
    protected $guarded = [];
    protected $appends = ['model_name'];
    public function feedbacks()
    {
        return $this->morphMany('App\Feedback', 'feedbacktable');
    }

    public function createdBy(){
        return $this->belongsTo(User::class,'created_by');
    }
    public function getModelNameAttribute(){
    	return 'contribution';
    }
}
