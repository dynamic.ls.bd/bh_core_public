<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Circle extends Model
{
	// use Notifiable;
    protected $guarded = [];
    protected $appends = ['title'];

    public function users(){
        return $this->belongsToMany(User::class,'circle_users','circle_id','user_id')->withPivot('relation','isAdmin');
    }

    public function createdBy(){
        return $this->belongsTo(User::class,'created_by');
    }

    public function getTitleAttribute($value){
    	$array = explode("_", $value);
    	if(count($array) == 3)
    		return ucwords($array[0].' '.$array[1]);
    	return ucwords($array[0]);
    }
}
