<?php

namespace App\Repositories\Contracts;

use App\HealthRecord;

/**
 * Interface HealthRecordRepository
 * @package App\Repositories\Contracts
 */
interface HealthRecordRepository extends BaseRepository
{

    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @param $user_id
     * @return mixed
     */
    public function getOwn($user_id);

    /**
     * @param array $input
     *
     * @return mixed
     */
    public function store(array $input);

    /**
     * @param HealthRecord $HealthRecord
     * @param array $input
     * @return mixed
     */
    public function update(HealthRecord $HealthRecord, array $input);

    /**
     * @param HealthRecord $HealthRecord
     * @return mixed
     */
    public function destroy(HealthRecord $HealthRecord);

    /**
     * @param array $ids
     *
     * @return mixed
     */
    public function batchDestroy(array $ids);
}
