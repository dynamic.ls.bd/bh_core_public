<?php

namespace App\Repositories;

use Exception;
use App\HealthRecord;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\Contracts\HealthRecordRepository;

/**
 * Class EloquentZoneRepository.
 */
class EloquentHealthRecordRepository extends EloquentBaseRepository implements HealthRecordRepository
{
    /**
     * EloquentHealthRecordRepository constructor.
     * @param HealthRecord $health_record
     */
    public function __construct(HealthRecord $health_record)
    {
        parent::__construct($health_record);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder|
     * \Illuminate\Database\Eloquent\Model|mixed
     */
    public function getAll()
    {
        return $this->model
            ->orderBy('date');
    }

    /**
     * @param $user_id
     * @return mixed
     */
    public function getOwn($user_id)
    {
        return $this->model
            ->where('user_id', $user_id)
            ->orderBy('date');
    }

    /**
     * @param array $input
     * @return \Illuminate\Database\Eloquent\Model|mixed
     * @throws GeneralException
     */
    public function store(array $input)
    {
        $health_record = $this->make($input);

        if (! $health_record->save()) {
            throw new GeneralException(__('Health records create failed'));
        }

        return $health_record;
    }

    /**
     * @param HealthRecord $health_record
     * @param array $input
     * @return HealthRecord|mixed
     * @throws GeneralException
     */
    public function update(HealthRecord $health_record, array $input)
    {
        if (! $health_record->update($input)) {
            throw new GeneralException('Health records update failed');
        }

        return $health_record;
    }

    /**
     * @param HealthRecord $health_record
     * @return bool|mixed
     * @throws GeneralException
     */
    public function destroy(HealthRecord $health_record)
    {
        if (! $health_record->delete()) {
            throw new GeneralException(__('Health records delete failed'));
        }

        return true;
    }

    /**
     * @param array $ids
     *
     * @throws \Exception|\Throwable
     *
     * @return mixed
     */
    public function batchDestroy(array $ids)
    {
        DB::transaction(function () use ($ids) {
            // This wont call eloquent events, change to destroy if needed
            if ($this->query()->whereIn('id', $ids)->delete()) {
                return true;
            }

            throw new GeneralException(__('exceptions.backend.health_records.delete'));
        });

        return true;
    }
}
