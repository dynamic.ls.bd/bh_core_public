<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class News extends Model
{
	use Notifiable;
    protected $guarded = [];

    public function author(){
    	return $this->belongsTo('App\User','user_id');
    }

}
