<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Observation extends Model
{
    protected $guarded = [];
    protected $appends = ['model_name'];

    public function feedbacks()
    {
        return $this->morphMany("App\Feedback", 'feedbacktable');
    }

    public function owner(){
        return $this->belongsTo(User::class, 'user_id');
    }
    public function getModelNameAttribute(){
    	return 'observation';
    }

    public function getTagsAttribute($value) {
        return array_map('trim', explode(',', $value));
    }

    public function setTagsAttribute($value) {
        $this->attributes['tags'] = implode(", ", $value);
    }
}
