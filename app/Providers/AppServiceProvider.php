<?php

namespace App\Providers;

use App\Repositories\Contracts\HealthRecordRepository;
use App\Repositories\EloquentHealthRecordRepository;
use Illuminate\Database\Schema\Builder;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            HealthRecordRepository::class,
            EloquentHealthRecordRepository::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Builder::defaultStringLength(191);
    }
}
