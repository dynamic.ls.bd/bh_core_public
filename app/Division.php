<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    public function districts(){
        return $this->hasMany('App\District');
    }

    public function upazilas()
    {
        return $this->hasManyThrough('App\Upazila', 'App\District');
    }
}
